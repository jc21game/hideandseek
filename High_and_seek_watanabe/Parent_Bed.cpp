#include "Parent_Bed.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Parent_Bed::Parent_Bed(IGameObject * parent)
	:IGameObject(parent, "Parent_Bed"), hModel_(-1)
{
}

//デストラクタ
Parent_Bed::~Parent_Bed()
{
}

//初期化
void Parent_Bed::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/Bed.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 1, 0), D3DXVECTOR3(6, 2, 12));
	AddCollider(collision);
	SetRotateY(180);

}

//更新
void Parent_Bed::Update()
{
}

//描画
void Parent_Bed::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Parent_Bed::Release()
{
}