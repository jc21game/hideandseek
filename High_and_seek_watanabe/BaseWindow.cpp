#include "BaseWindow.h"
#include "Engine/ResouceManager/Image.h"
#include "Gimmick.h"
#include "Number.h"
#include "Entry.h"


//コンストラクタ
BaseWindow::BaseWindow(IGameObject * parent)
	:IGameObject(parent, "BaseWindow"), hPict_(-1)
{
}

//デストラクタ
BaseWindow::~BaseWindow()
{
}

//初期化
void BaseWindow::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/DoorGimmickTurn/BaseWindow.png");
	assert(hPict_ >= 0);


	position_ = D3DXVECTOR3(-128.0f, -62.0f, 0.5);
}

//更新
void BaseWindow::Update()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		CreateGameObject<Entry>(this);
	}
}

//描画
void BaseWindow::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void BaseWindow::Release()
{
}



