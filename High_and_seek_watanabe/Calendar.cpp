#include "Calendar.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Calendar::Calendar(IGameObject * parent)
	:IGameObject(parent, "Calendar"), hModel_(-1)
{
}

//デストラクタ
Calendar::~Calendar()
{
}

//初期化
void Calendar::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/Calendar.fbx");
	assert(hModel_ >= 0);

	SetScale(2, 2, 1);
}

//更新
void Calendar::Update()
{
}

//描画
void Calendar::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Calendar::Release()
{
}