#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Cursor : public IGameObject
{

	int hPict_;

public:
	//コンストラクタ
	Cursor(IGameObject* parent);

	//デストラクタ
	~Cursor();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

