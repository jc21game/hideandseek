#include "Curtain.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Curtain::Curtain(IGameObject * parent)
	:IGameObject(parent, "Curtain"), hModel_(-1)
{
}

//デストラクタ
Curtain::~Curtain()
{
}

//初期化
void Curtain::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/Curtain.fbx");
	assert(hModel_ >= 0);

}

//更新
void Curtain::Update()
{
}

//描画
void Curtain::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Curtain::Release()
{
}