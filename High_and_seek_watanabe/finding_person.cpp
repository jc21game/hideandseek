#include "finding_person.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"
#include "XINPUT.h"
#include "Wall.h"

#include <time.h>
#include "furniture.h"
#include "PlayScene.h"

//コンストラクタ
finding_person::finding_person(IGameObject * parent)
	:IGameObject(parent, "finding_person"), hModel_(-1), find_sec(0), searchStatus(0), result_sec(0)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
finding_person::~finding_person()
{
}

//初期化
void finding_person::Initialize()
{
	//モデルデータのロード
	/*hModel_ = Model::Load("data/Model/player.fbx");
	assert(hModel_ >= 0);*/

	//画像データのロード
	hPict_[0] = Image::Load("Data/Search.png");
	assert(hPict_[0] >= 0);

	hPict_[1] = Image::Load("Data/Search_None.png");
	assert(hPict_[1] >= 0);

	//スポーン地点を決めなければいけない。
	//position_ = D3DXVECTOR3(-37, 1, -37);

	//カメラ
	Camera* pCamera_ = CreateGameObject<Camera>(this);
	assert((pCamera_) != nullptr);
	pCamera_->SetPosition(D3DXVECTOR3(0, 3.5f, 0));
	pCamera_->SetTarget(D3DXVECTOR3(0, 0, 10));

	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, -1, 0), D3DXVECTOR3(1, 3, 1));
	AddCollider(collision);


	////ポジション受け取るやつ
	//D3DXVECTOR3 pos = ((Stage*)SceneManager::GetCurrentScene()->
	//	FindChildObject("hide_person"))->GetPosition();
}

//更新
void finding_person::Update()
{
	//プレイヤーの移動動作//プレイヤーの移動動作
	if (searchStatus != 1)
	{
		finding_person_move();
	}
	//当たり判定の処理
	HitTest();
	Search_finding();

	if (result_sec > 240)
	{
		search_result = false;
		result_sec = 0;
	}
	if (search_result) result_sec++;
}

void finding_person::finding_person_move()
{
	//動く前に直前の位置を取る
	prePos = position_;

	//コントローラ
	D3DXVECTOR3 leftStick = Input::GetPadStickL();
	D3DXVECTOR3 rightStick = Input::GetPadStickR();

	move(leftStick);	//キー、コントローラの処理
	CamMove(rightStick);

	//現在の位置 - 前にいた位置でベクトルの角度を取る
	//現在と前の位置の「差」
	D3DXVECTOR3 move = position_;// - prePos;

}

void finding_person::move(D3DXVECTOR3 &stick)
{

	//////////////////////////////////////////////////////////////

		//回転行列を用意
	D3DXMATRIX matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(rotate_.y));

	D3DXVECTOR3 vecForward = D3DXVECTOR3(0, 0, 0.3f);			//奥移動ベクトル
	D3DXVec3TransformCoord(&vecForward, &vecForward, &matRot);

	D3DXVECTOR3 vecRight = D3DXVECTOR3(0.3f, 0, 0);				//右移動ベクトル
	D3DXVec3TransformCoord(&vecRight, &vecRight, &matRot);

	////////////////////////////////////////////////////////////////

	//押してる間ダッシュ
	/*if (Input::IsKey(DIK_E))
	{
		Debuff = 2.0f;
	}
	else
	{
		Debuff = 1.0f;
	}*/
	if (position_.x >= -79.0f && position_.x <= 79.0f &&
		position_.z >= -79.0f  && position_.z <= 79.0f &&
		position_.y >= -160.0f && position_.y <= 160.0f)
	{
		if (Input::IsKey(DIK_W))	position_ += vecForward * Debuff;
		if (Input::IsKey(DIK_S))	position_ -= vecForward * Debuff;
		if (Input::IsKey(DIK_A))	position_ -= vecRight * Debuff;
		if (Input::IsKey(DIK_D))	position_ += vecRight * Debuff;
		if (Input::IsKey(DIK_SPACE))position_.y += 1.0f;
		if (Input::IsKey(DIK_LSHIFT))position_.y -= 1.0f;

		//移動コントローラー
		if (stick.z > 0)	position_ += vecForward * Debuff;
		if (stick.z < 0)	position_ -= vecForward * Debuff;
		if (stick.x > 0)	position_ += vecRight * Debuff;
		if (stick.x < 0)	position_ -= vecRight * Debuff;
		if (Input::IsPadButton(XINPUT_GAMEPAD_A))position_.y += 1.0f;
		if (Input::IsPadButton(XINPUT_GAMEPAD_B))position_.y -= 1.0f;
	}
	else
	{
		if (position_.z <= -79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, -79.0f);
		if (position_.z >= 79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, 79.0f);
		if (position_.x <= -79.0f) 		position_ = D3DXVECTOR3(-79.0f, position_.y, position_.z);
		if (position_.x >= 79.0f)		position_ = D3DXVECTOR3(79.0f, position_.y, position_.z);
	}

}

//カメラ回転
void finding_person::CamMove(D3DXVECTOR3 &stick)
{
	//ゲームパッド
	if (stick.y > 0)		rotate_.x -= 1.0f;
	if (stick.y < 0)		rotate_.x += 1.0f;
	if (stick.x < 0)		rotate_.y -= 1.0f;
	if (stick.x > 0)		rotate_.y += 1.0f;

	//キーボード
	if (Input::IsKey(DIK_UP))		rotate_.x -= 1.0f;
	if (Input::IsKey(DIK_DOWN))		rotate_.x += 1.0f;
	if (Input::IsKey(DIK_LEFT))		rotate_.y -= 1.0f;
	if (Input::IsKey(DIK_RIGHT))	rotate_.y += 1.0f;
}

void finding_person::HitTest()
{
	Wall* pWall = (Wall*)FindObject("Wall");    //ステージオブジェクトを探す
	int hWall = pWall->GetModelHandle();    //モデル番号を取得

	//右
	RayCastData right;
	right.start = position_;              //レイの発射位置
	right.dir = D3DXVECTOR3(1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &right); //レイを発射

	  //レイが当たったら
	if (right.hit)
	{
		if (right.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//左
	RayCastData left;
	left.start = position_;              //レイの発射位置
	left.dir = D3DXVECTOR3(-1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &left); //レイを発射

	  //レイが当たったら
	if (left.hit)
	{
		if (left.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//上	
	RayCastData forward;
	forward.start = position_;              //レイの発射位置
	forward.dir = D3DXVECTOR3(0, 0, 1);   //レイの方向
	Model::RayCast(hWall, &forward); //レイを発射

	  //レイが当たったら
	if (forward.hit)
	{
		if (forward.dist < 1.0f)
		{
			//position_ -= vecForward;
			position_ = prePos;
		}
	}

	//下
	RayCastData backward;
	backward.start = position_;              //レイの発射位置
	backward.dir = D3DXVECTOR3(0, 0, -1);    //レイの方向
	Model::RayCast(hWall, &backward); //レイを発射

	  //レイが当たったら
	if (backward.hit)
	{
		if (backward.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

}


//描画
void finding_person::Draw()
{
	//Model::SetMatrix(hModel_, _worldMatrix);
	//Model::Draw(hModel_);
	if (find_sec > 1 && searchStatus == 1)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (g.ScWidth / 2) - 256, (g.ScHeight / 2) - 64, 0);

		Image::SetMatrix(hPict_[0], m);
		Image::Draw(hPict_[0]);
	}
	if (search_result)	Image::Draw(hPict_[1]);
}

//開放
void finding_person::Release()
{
}

void finding_person::OnCollision(IGameObject * pTarget)
{
	//HidePointの上にいる時、Eキーで隠す
	//Eキーで物が入ってた場合、"物がある" → "物を取った"(物がないとは別）状態にする
	//物が取られていた場合Resultシーン（勝利）にする。

	position_ = prePos;
}


//探す側のサーチ
void finding_person::Search_finding()
{
	if (Input::IsKey(DIK_Q))
	{
		if (!flag_pBed) Search_ParentBed();
		if (!flag_cBed)	Search_ChildBed();
		if (!flag_fridge)	Search_Fridge();
		if (!flag_BookShelf)	Search_BookShelf();
	}
	else
	{
		find_sec = 0;
		searchStatus = 0;
	}
}

void finding_person::Search_ParentBed()
{
	if (position_.x >= 30 && position_.x <= 35 &&
		position_.z <= 30 && position_.z >= 25)
	{
		if (find_sec > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			Parent_Bed* pParentBed = (Parent_Bed*)FindObject("Parent_Bed");
			if (Have == pParentBed->GetStatus())
			{
				//探す側は家具があったら、GetStatusで見る。
				//物があったらゲームを終了させる。
				SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
				g.timeRecord_tens = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_tens();
				g.timeRecord_sec = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_sec();
				g.winFlag_ = true;
				pSceneManager->ChangeScene(SCENE_ID_RESULT);
			}

			if (Time == pParentBed->GetStatus() && Flag_time == 0)
			{
				Flag_time = 1;
				((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(1);
			}

			if (Have != pParentBed->GetStatus())
			{
				search_result = true;
			}
			flag_pBed = true;
		}
		else
		{
			searchStatus = 1;
		}
		find_sec++;
	}
}


void finding_person::Search_ChildBed()
{
	if (position_.x >= 30 && position_.x <= 35 &&
		position_.z <= -22 && position_.z >= -30)
	{
		if (find_sec > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			Child_Bed* pChildBed = (Child_Bed*)FindObject("Child_Bed");
			if (Have == pChildBed->GetStatus())
			{
				//探す側は家具があったら、GetStatusで見る。
				//物があったらゲームを終了させる。
				SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
				g.timeRecord_tens = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_tens();
				g.timeRecord_sec = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_sec();
				g.winFlag_ = true;
				pSceneManager->ChangeScene(SCENE_ID_RESULT);
			}

			if (Time == pChildBed->GetStatus() && Flag_time == 0)
			{
				Flag_time = 1;
				((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(1);
			}

			if (Have != pChildBed->GetStatus())
			{
				search_result = true;
			}
			flag_cBed = true;
		}
		else
		{
			searchStatus = 1;
		}
		find_sec++;
	}
}

void finding_person::Search_Fridge()
{
	if (position_.x <= -15 && position_.x >= -23 &&
		position_.z <= -15 && position_.z >= -20.5f)
	{
		if (find_sec > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			fridge* pfridge = (fridge*)FindObject("fridge");
			if (Have == pfridge->GetStatus())
			{
				//探す側は家具があったら、GetStatusで見る。
				//物があったらゲームを終了させる。
				SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
				g.timeRecord_tens = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_tens();
				g.timeRecord_sec = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_sec();
				g.winFlag_ = true;
				pSceneManager->ChangeScene(SCENE_ID_RESULT);
			}

			if (Time == pfridge->GetStatus() && Flag_time == 0)
			{
				Flag_time = 1;
				((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(1);
			}

			if (Have != pfridge->GetStatus())
			{
				search_result = true;
			}
			flag_fridge = true;
		}
		else
		{
			searchStatus = 1;
		}
		find_sec++;
	}
}

void finding_person::Search_BookShelf()
{
	if (position_.x >= 34 && position_.x <= 40 &&
		position_.z >= 2 && position_.z <= 9)
	{
	if (find_sec > 60) //1秒経ったら、
	{
		//探索中画像を消す
		searchStatus = 0;
		BookShelf* pBookShelf = (BookShelf*)FindObject("BookShelf");
		if (Have == pBookShelf->GetStatus())
		{
			//探す側は家具があったら、GetStatusで見る。
			//物があったらゲームを終了させる。
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			g.timeRecord_tens = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_tens();
			g.timeRecord_sec = ((PlayScene*)SceneManager::GetCurrentScene())->GetTime_sec();
			g.winFlag_ = true;
			pSceneManager->ChangeScene(SCENE_ID_RESULT);
		}

		if (Time == pBookShelf->GetStatus() && Flag_time == 0)
		{
			Flag_time = 1;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(1);
		}

		if (Have != pBookShelf->GetStatus())
		{
			search_result = true;
		}
		flag_BookShelf = true;
	}
	else
	{
		searchStatus = 1;
	}
	find_sec++;
	}
}
