#include <assert.h>
#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), hPict_(-1), fade_In(75)
{
}

//初期化
void TitleScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);

	//画像データのロード
	hPict_ = Image::Load("Data/Logo/title2.png"); 
	assert(hPict_ >= 0);
}

//更新
void TitleScene::Update()
{
	//スペースが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//プレイシーンへ移行
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	if (fade_In != 255) fade_In++;

}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::AlphaDraw(hPict_, fade_In);
}

//開放
void TitleScene::Release()
{
}