#pragma once
#include "House.h"
#include "Wall.h"
#include "Ceiling.h"

#include "Chair.h"
#include "Clock.h"
#include "Shelf.h"
#include "Sofa.h"
#include "Tv.h"
#include "Kitchen.h"
#include "Parent_Bed.h"
#include "Child_Bed.h"
#include "foliage_plant.h"
#include "WashingMachine.h"
#include "TrashCan.h"
#include "longTrashCan.h"
#include "LaundryBasket.h"
#include "diningtable.h"
#include "Cushion.h"
#include "fridge.h"
#include "Curtain.h"
#include "CurtainFleam.h"
#include "Toilet.h"
#include "BookShelf.h"
#include "Calendar.h"

enum {
	None,
	Have,
	Slow,
	Time,
	Search
};