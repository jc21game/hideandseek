#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class diningtable : public IGameObject
{
	int hModel_;
	int status;
public:
	//コンストラクタ
	diningtable(IGameObject* parent);

	//デストラクタ
	~diningtable();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//物のステータス
	void SetStatus(int change) { status = change; }
	int GetStatus() { return status; };
};