#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Wall : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//デストラクタ
	~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	int GetModelHandle() { return hModel_; }
};