#pragma once
#include "gameObject/gameObject.h"
#include "sceneManager/sceneManager.h"
#include "DirectX\Direct3D.h"
#include "DirectX\Input.h"


//安全にメモリを開放するためのマクロ
#define SAFE_DELETE(p) {if ((p)!=nullptr) { delete (p); (p)=nullptr;}}
#define SAFE_DELETE_ARRAY(p) {if ((p)!=nullptr) { delete[] (p); (p)=nullptr;}}

struct Global
{
	bool movieFlag_;
	int ScWidth = GetPrivateProfileInt("SCREEN", "Width", 800, ".\\Data\\setup.ini");
	int ScHeight = GetPrivateProfileInt("SCREEN", "Height", 600, ".\\Data\\setup.ini");
	int timeRecord_tens;
	int timeRecord_sec;
	bool winFlag_;
};

extern Global g;

//ムービーで本当は使う・・・。