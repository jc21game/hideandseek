#pragma once
#include <d3dx9.h>
#include <string>
#include "../global.h"

class Text
{
	//フォントオブジェクト
	LPD3DXFONT _pFont;

	//カラー
	DWORD _color;

public:
	//コンストラクタ
	Text(std::string fontName, int size);

	//デストラクタ
	~Text();

	void SetColor(DWORD r, DWORD g, DWORD b, DWORD alpha);

	void Draw(int x, int y, std::string text);
};