#pragma once
#include <d3dx9.h>
#include <string>
#include "../global.h"


//2D画像の描画クラス
class Sprite
{
	//スプライト
	LPD3DXSPRITE _pSprite;

	//テクスチャ
	LPDIRECT3DTEXTURE9 _pTexture;

	//画像表示のアルファブレンド、フェードイン用
	int alphaB;

public:
	//コンストラクタ
	Sprite();

	//デストラクタ
	~Sprite();

	//画像ファイルの読み込み
	//引数：fileName	ファイル名
	//戻値：成功／失敗
	HRESULT Load(std::string fileName);

	//描画
	//引数：matrix	ワールド行列
	//引数：rect	切り抜き範囲
	void Draw(D3DXMATRIX& matrix, RECT* rect);

	void AlphaDraw(D3DXMATRIX& matrix, RECT* rect,int alpha);

	//テクスチャのサイズ
	//戻値：テクスチャの幅と高さ
	D3DXVECTOR2 GetTextureSize();
};