#include "Input.h"
#pragma comment(lib,"Xinput.lib")

namespace Input
{
	//ウィンドウハンドル
	HWND	_hWnd;

	//DirectInputオブジェクト
	LPDIRECTINPUT8			_pDInput;

	//キーボード
	LPDIRECTINPUTDEVICE8	_pKeyDevice;	//デバイスオブジェクト
	BYTE _keyState[256];					//現在の各キーの状態
	BYTE _prevKeyState[256];				//前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8	_pMouseDevice;	//デバイスオブジェクト
	DIMOUSESTATE _mouseState;				//マウスの状態
	DIMOUSESTATE _prevMouseState;			//前フレームのマウスの状態

	//コントローラー
	const int MAX_PAD_NUM = 4;
	XINPUT_STATE _controllerState[MAX_PAD_NUM];
	XINPUT_STATE _prevControllerState[MAX_PAD_NUM];




	//初期化
	void Initialize(HWND hWnd)
	{
		//ウィンドウハンドル
		_hWnd = hWnd;

		//DirectInput本体
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
			IID_IDirectInput8, (VOID**)&_pDInput, nullptr);

		//キーボード
		_pDInput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, nullptr);
		_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		_pKeyDevice->SetCooperativeLevel(_hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

		//マウス
		_pDInput->CreateDevice(GUID_SysMouse, &_pMouseDevice, nullptr);
		_pMouseDevice->SetDataFormat(&c_dfDIMouse);
		_pMouseDevice->SetCooperativeLevel(_hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	}


	//更新
	void Update()
	{
		//キーボード
		_pKeyDevice->Acquire();
		memcpy(_prevKeyState, _keyState, sizeof(_keyState));
		_pKeyDevice->GetDeviceState(sizeof(_keyState), &_keyState);

		//マウス
		_pMouseDevice->Acquire();
		memcpy(&_prevMouseState, &_mouseState, sizeof(_mouseState));
		_pMouseDevice->GetDeviceState(sizeof(_mouseState), &_mouseState);

		//コントローラー
		for (int i = 0; i < MAX_PAD_NUM; i++)
		{
			memcpy(&_prevControllerState[i], &_controllerState[i], sizeof(_controllerState[i]));
			XInputGetState(i, &_controllerState[i]);
		}

	}



	//開放
	void Release()
	{
		SAFE_RELEASE(_pMouseDevice);
		SAFE_RELEASE(_pKeyDevice);
		SAFE_RELEASE(_pDInput);
	}



	/////////////////////////////　キーボード情報取得　//////////////////////////////////

	//キーが押されているか調べる
	bool IsKey(int keyCode)
	{
		//押してる
		if (_keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}


	//キーを今押したか調べる（押しっぱなしは無効）
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) && !(_prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}


	//キーを今放したか調べる
	bool IsKeyUp(int keyCode)
	{
		//今押してなくて、前回は押してる
		if (!IsKey(keyCode) && _prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}


	/////////////////////////////　マウス情報取得　//////////////////////////////////

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (_mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(_prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && _prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;
		GetCursorPos(&mousePos);
				D3DXVECTOR3 result = D3DXVECTOR3(mousePos.x, mousePos.y, 0);
		return result;
	}


	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result(_mouseState.lX, _mouseState.lY, _mouseState.lZ);
		return result;
	}


	/////////////////////////////　コントローラー情報取得　//////////////////////////////////

	//コントローラーのボタンが押されているか調べる
	bool IsPadButton(int buttonCode, int padID)
	{
		if (_controllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true; //押してる
		}
		return false; //押してない
	}

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsPadButtonDown(int buttonCode, int padID)
	{
		//今は押してて、前回は押してない
		if (IsPadButton(buttonCode, padID) && !(_prevControllerState[padID].Gamepad.wButtons & buttonCode))
		{
			return true;
		}
		return false;
	}

	//コントローラーのボタンを今放したか調べる
	bool IsPadButtonUp(int buttonCode, int padID)
	{
		//今押してなくて、前回は押してる
		if (!IsPadButton(buttonCode, padID) && _prevControllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}


	float GetAnalogValue(int raw, int max, int deadZone)
	{
		float result = (float)raw;

		if (result > 0)
		{
			//デッドゾーン
			if (result < deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result - deadZone) / (max - deadZone);
			}
		}

		else
		{
			//デッドゾーン
			if (result > -deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result + deadZone) / (max - deadZone);
			}
		}

		return result;
	}


	//左スティックの傾きを取得
	D3DXVECTOR3 GetPadStickL(int padID)
	{
		float x = GetAnalogValue(_controllerState[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		float z = GetAnalogValue(_controllerState[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		return D3DXVECTOR3(x, 0, z);
	}

	//右スティックの傾きを取得
	D3DXVECTOR3 GetPadStickR(int padID)
	{
		float x = GetAnalogValue(_controllerState[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		float y = GetAnalogValue(_controllerState[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		return D3DXVECTOR3(x, y, 0);
	}

	//左トリガーの押し込み具合を取得
	float GetPadTrrigerL(int padID)
	{
		return GetAnalogValue(_controllerState[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//右トリガーの押し込み具合を取得
	float GetPadTrrigerR(int padID)
	{
		return GetAnalogValue(_controllerState[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//振動させる
	void SetPadVibration(int l, int r, int padID)
	{
		XINPUT_VIBRATION vibration;
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		vibration.wLeftMotorSpeed = l; // 左モーターの強さ
		vibration.wRightMotorSpeed = r;// 右モーターの強さ
		XInputSetState(padID, &vibration);
	}










}