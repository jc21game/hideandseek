#pragma once
#include "../global.h"
#include "sceneID.h"

//シーン切り替えを担当するオブジェクト
class SceneManager : public IGameObject
{
	static IGameObject* pCurrentScene_;
public:

	//コンストラクタ
	//引数：parent	親オブジェクト（基本的にゲームマネージャー）
	SceneManager(IGameObject* parent);

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：next	次のシーンのID
	void ChangeScene(SCENE_ID next);

	static IGameObject* GetCurrentScene()
	{
		return pCurrentScene_;	
	}

private:
	SCENE_ID _currentSceneID;	//現在のシーン
	SCENE_ID _nextSceneID;		//次のシーン
};