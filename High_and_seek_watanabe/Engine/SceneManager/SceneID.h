#pragma once

enum SCENE_ID
{
	SCENE_ID_SPLASH = 0,
	SCENE_ID_TITLE = 1,
	SCENE_ID_PLAY = 2,
	SCENE_ID_LOGO = 3,
	SCENE_ID_RESULT = 4,
};