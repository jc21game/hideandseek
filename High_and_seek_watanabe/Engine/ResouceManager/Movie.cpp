#include "Movie.h"



namespace Movie
{
	REFTIME length;

	IGraphBuilder *pGB = NULL;
	ICaptureGraphBuilder2 *pCGB2 = NULL;
	IBaseFilter *pVMR9 = NULL;
	IBaseFilter *pSource = NULL;
	IVMRFilterConfig *pVMRCfg = NULL;
	IVMRWindowlessControl *pVMRWndCont = NULL;
	IMediaControl *pMediaCont = NULL;
	
	IMediaPosition *pMediaPosition = NULL;
	

	void Initialize(HWND hWnd)
	{
		CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

		//フィルタグラフマネージャーの作成
		CoCreateInstance
		(
			CLSID_FilterGraph,		//インターフェイスを含むクラスIDを指定,IGraphBuilderを取得するときはコレしかない無条件でこれを書いてね
			NULL,					//COMオブジェクトの内報状態を指定するフラグ　NULL でいいらしい理由はしらん
			CLSCTX_INPROC_SERVER,	//生成するCOMの実装ソースがあるDLLの場所を表すらしい
			IID_IGraphBuilder,		//欲しいインターフェースの固有ID
			(void**)&pGB			//ここにIGraphBuilderインターフェイスへのポインタが返る
		);

		//VMR9フィルタの作成	レンダリングを担当する
		CoCreateInstance
		(
			CLSID_VideoMixingRenderer9,		//察しのいい奴はもうわかるな上と同じこれも仕様！	
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IBaseFilter,
			(void**)&pVMR9
		);

		//グラフフィルターに追加！
		pGB->AddFilter(pVMR9, L"VMR9");

		//ウィンドウモードの指定
		pVMR9->QueryInterface(IID_IVMRFilterConfig9, (void**)&pVMRCfg); //VMR9からインターフェイスを取得
		pVMRCfg->SetRenderingMode(VMRMode_Windowless);//モードの指定
		pVMRCfg->Release();

		//ウィンドウハンドルの指定
		pVMR9->QueryInterface(IID_IVMRWindowlessControl9,(void**)&pVMRWndCont);
		pVMRWndCont->SetVideoClippingWindow(hWnd); 		
		pVMRWndCont->Release();

		//ウィンドウのサイズの決定
		LONG width, hight;
		RECT srcRect, destRect;
		pVMRWndCont->GetNativeVideoSize(&width, &hight, NULL, NULL);	//動画のサイズを取得
		SetRect(&srcRect, 0, 0, width,hight);							//ソースサイズ値を保持
		GetClientRect(hWnd, &destRect);									//ウィンドウのクライアントサイズを取得
		pVMRWndCont->SetVideoPosition(&srcRect, &destRect);				//貼り付け元と先のサイズを指定

		
	}
	void Load(WCHAR* wFileName)
	{
		// ソースフィルタの作成と登録
		//第1引数はソースファイルの名前 第2引数は「フィルタの名前」
		pGB->AddSourceFilter(wFileName, wFileName, &pSource);
		CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCGB2);
		
		//フィルタの接続のための準備
		pCGB2->SetFiltergraph(pGB);

		//フィルタの接続
		pCGB2->RenderStream(0, 0, pSource, 0, pVMR9);
		pCGB2->RenderStream(0, &MEDIATYPE_Audio, pSource, 0, 0);

		//フィルタグラフの実行
		pGB->QueryInterface(IID_IMediaControl, (void**)&pMediaCont);

		pGB->QueryInterface(IID_IMediaPosition,
			(LPVOID *)&pMediaPosition);

	}

	void Play()
	{
		pMediaCont->Run();
	}

	void Stop()
	{
		pMediaCont->Stop();
	}

	void Pause()
	{
		pMediaCont->Pause();
	}
	
	OAFilterState MovieState()
	{
		OAFilterState	pfs;
		
		//  pfsには次の値が入る
		//	State_Stopped 停止中
		//	State_Paused　一時停止中
		//	State_Running　再生中
		
		pMediaCont->GetState(Movie::GetMovieLength() * 1000, &pfs);

		return pfs;
	}

	int GetMovieLength()
	{
		pMediaPosition->get_Duration(&length);

		return length * 1000;
	}



	void Release()
	{
		pMediaCont->Release();
		pVMR9->Release();
		pCGB2->Release();
		pGB->Release();

		CoUninitialize();
	}
}