#pragma once
#include <windows.h>
#include <objbase.h>
#include <tchar.h>
#include <d3d9.h>
#include <dshow.h>
#include <Vmr9.h>
#include <string>

#pragma comment(lib, "Strmiids.lib")

#define _WIN32_DCOM     // CoInitializeEx関数の呼び出しに必要


namespace Movie
{
	void Initialize(HWND hWnd);

	void Load(WCHAR* wFileName);

	void Play();

	void Stop();

	void Pause();

	void Release();

	OAFilterState MovieState();

	int GetMovieLength();

}