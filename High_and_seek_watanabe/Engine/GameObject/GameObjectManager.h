#pragma once
#include <string>

class IGameObject;
class SceneManager;

//ゲームオブジェクトを管理する関数達
namespace GameObjectManager 
{
	//初期化
	void Initialize();

	//更新
	void Update();

	//描画
	void Draw();

	//開放
	void Release();

	//ルートの子供としてオブジェクトを追加
	//引数：追加するオブジェクト（実態はSceneManager）
	void PushBackChild(IGameObject* obj);

	//名前でオブジェクトを検索
	//引数：探したい名前
	//引数：見つけたオブジェクトのアドレス（見つからなければnullptr）
	IGameObject* FindChildObject(const std::string& name);

};
