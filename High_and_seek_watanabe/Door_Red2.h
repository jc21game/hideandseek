#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Door_Red2 : public IGameObject
{
	int hPict_;

public:
	//コンストラクタ
	Door_Red2(IGameObject* parent);

	//デストラクタ
	~Door_Red2();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

