#include "Door_Red2.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Door_Red2::Door_Red2(IGameObject * parent)
	:IGameObject(parent, "Door_Red2"), hPict_(-1)
{
}

//デストラクタ
Door_Red2::~Door_Red2()
{
}

//初期化
void Door_Red2::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/DoorGimmickTurn/Door_Red2.png");
	assert(hPict_ >= 0);
}

//更新
void Door_Red2::Update()
{
	if(Input::IsKey(DIK_2))
	{
		position_ = D3DXVECTOR3(336.0f, 400.0f, 0);
	}

	if (Input::IsKey(DIK_3))
	{
		position_ = D3DXVECTOR3(480.0f, 400.0f, 0);
	}

	if (Input::IsKey(DIK_4))
	{
		position_ = D3DXVECTOR3(336.0f, 560.0f, 0);
	}

}

//描画
void Door_Red2::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void Door_Red2::Release()
{
}



