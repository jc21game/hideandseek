#include "Cursor.h"
#include "Entry.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Cursor::Cursor(IGameObject * parent)
	:IGameObject(parent, "Cursor"), hPict_(-1)
{
}

//デストラクタ
Cursor::~Cursor()
{
}

//初期化
void Cursor::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/DoorGimmickTurn/IconCursor.png");
	assert(hPict_ >= 0);

	position_ = D3DXVECTOR3(930.0f, 130.0f, 0);
}

//更新
void Cursor::Update()
{
	if (position_.y < 320)
	{
		if (Input::IsKeyDown(DIK_1) || Input::IsKeyDown(DIK_2) || Input::IsKeyDown(DIK_3) 
			|| Input::IsKeyDown(DIK_4) || Input::IsKeyDown(DIK_5) || Input::IsKeyDown(DIK_6))
		{
			position_.y += 160.0f;
		}

	}

	if (Input::IsKeyDown(DIK_BACKSPACE))
	{
		position_.y -= 160.0f;
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		KillMe();
	}
}

//描画
void Cursor::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void Cursor::Release()
{
}



