#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"
#include "Engine/ResouceManager/Image.h"
#include "furniture.h"
#include "HidePoint.h"
#include "Gimmick.h"
#include "finding_person.h"
#include "PlayScene.h"
#include "Engine/global.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), stage_Gimmick(false), gimmick_turn(false), find_turn(false)
{
	for (int i = 0; i < 7; i++) 
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
		StageObject();

		//Player* pPlayer_ = CreateGameObject<Player>(this);
		//pPlayer_->SetPosition(D3DXVECTOR3(-77, 1, -77));

		hide_person* pHidePlayer = CreateGameObject<hide_person>(this);

		//finding_person* pFindingPlayer = CreateGameObject<finding_person>(this);
		//pFindingPlayer->SetPosition(D3DXVECTOR3(-77, 1, -77));

}

void Stage::StageObject()
{
	furniture();
	ActionPoint();

	House* pHouse;
	pHouse = CreateGameObject<House>(this);
	
	Wall* pWall;
	pWall = CreateGameObject<Wall>(this);

	Ceiling* pCeiling;
	pCeiling = CreateGameObject<Ceiling>(this);
	pCeiling->SetPosition(D3DXVECTOR3(0, 40, 0));
}

void Stage::ActionPoint()
{
	HidePoint* pHide_ParentBed;
	pHide_ParentBed = CreateGameObject<HidePoint>(this);
	pHide_ParentBed->SetPosition(D3DXVECTOR3(33, 1, 26));

	HidePoint* pHide_ChildBed;
	pHide_ChildBed = CreateGameObject<HidePoint>(this);
	pHide_ChildBed->SetPosition(D3DXVECTOR3(33, 1, -26));

	HidePoint* pHide_Fridge;
	pHide_Fridge = CreateGameObject<HidePoint>(this);
	pHide_Fridge->SetPosition(D3DXVECTOR3(-19, 1, -20));

	HidePoint* pHide_BookShelf;
	pHide_BookShelf = CreateGameObject<HidePoint>(this);
	pHide_BookShelf->SetPosition(D3DXVECTOR3(37.5, 1, 4.5f));


}

void Stage::furniture()
{
	/////隠せる家具/////
	Parent_Bed* pParentBed;
	pParentBed = CreateGameObject<Parent_Bed>(this);
	pParentBed->SetPosition(D3DXVECTOR3(33, 1, 33));

	Child_Bed* pChildBed;
	pChildBed = CreateGameObject<Child_Bed>(this);
	pChildBed->SetPosition(D3DXVECTOR3(33, 1, -33));
	
	//予定
	fridge* pFridge;
	pFridge = CreateGameObject<fridge>(this);
	pFridge->SetPosition(D3DXVECTOR3(-23, 1, -20));

	//本棚 に一つ
	BookShelf* pBookShelf;
	pBookShelf = CreateGameObject<BookShelf>(this);
	pBookShelf->SetPosition(D3DXVECTOR3(37.5f, 1.1, 1.5f));

	BookShelf* pBookShelf_two;
	pBookShelf_two = CreateGameObject<BookShelf>(this);
	pBookShelf_two->SetPosition(D3DXVECTOR3(33.5f, 1.1, 1.5f));

	BookShelf* pBookShelf_three;
	pBookShelf_three = CreateGameObject<BookShelf>(this);
	pBookShelf_three->SetPosition(D3DXVECTOR3(29.5f, 1.1, 1.5f));

	BookShelf* pBookShelf_four;
	pBookShelf_four = CreateGameObject<BookShelf>(this);
	pBookShelf_four->SetPosition(D3DXVECTOR3(25.5f, 1.1, 1.5f));

	///////////////////

	Chair* pChair;
	pChair = CreateGameObject<Chair>(this);
	pChair->SetPosition(D3DXVECTOR3(-6, 1, -27));
	pChair->SetScale(D3DXVECTOR3(0.5f, 0.5f, 0.5f));
	pChair->SetRotateY(180.0f);

	Clock* pClock;
	pClock = CreateGameObject<Clock>(this);
	pClock->SetPosition(D3DXVECTOR3(10, 16, -10.5));
	pClock->SetRotateY(180.0f);

	Sofa* pSofa;
	pSofa = CreateGameObject<Sofa>(this);
	pSofa->SetPosition(D3DXVECTOR3(13, 1, -25));
	pSofa->SetRotateY(180.0f);

	/*Shelf* pShelf;
	pShelf = CreateGameObject<Shelf>(this);
	pShelf->SetPosition(D3DXVECTOR3(21, 1, -40));
	pShelf->SetRotateY(180.0f);*/

	TV* pTv;
	pTv = CreateGameObject<TV>(this);
	pTv->SetPosition(D3DXVECTOR3(15, 1.5, -36));
	pTv->SetRotateY(90.0f);
	
	Kitchen* pKitchen;
	pKitchen = CreateGameObject<Kitchen>(this);
	pKitchen->SetPosition(D3DXVECTOR3(-15, 0, -35));
	
	foliage_plant* pPlant;
	pPlant = CreateGameObject<foliage_plant>(this);
	pPlant->SetPosition(D3DXVECTOR3(20, 2, -12));

	Cushion* pCushion;
	pCushion = CreateGameObject<Cushion>(this);
	pCushion->SetPosition(D3DXVECTOR3(-39, 1, -50));

	LaundryBasket* pBasket;
	pBasket = CreateGameObject<LaundryBasket>(this);
	pBasket->SetPosition(D3DXVECTOR3(-39, 1, -54));

	longTrashCan* plongCan;
	plongCan = CreateGameObject<longTrashCan>(this);
	plongCan->SetPosition(D3DXVECTOR3(-39, 1, -58));

	TrashCan* pCan;
	pCan = CreateGameObject<TrashCan>(this);
	pCan->SetPosition(D3DXVECTOR3(7, 1.2f, -12));

	diningtable* pTable;
	pTable = CreateGameObject<diningtable>(this);
	pTable->SetPosition(D3DXVECTOR3(-5, 0, -30));

	WashingMachine* pWashing;
	pWashing = CreateGameObject<WashingMachine>(this);
	pWashing->SetPosition(D3DXVECTOR3(-8, 3, 37));
	pWashing->SetScale(1.5f, 1.2f, 1.5f);
	pWashing->SetRotateY(180.0f);

	Curtain* pCurtain;
	pCurtain = CreateGameObject<Curtain>(this);
	pCurtain->SetPosition(D3DXVECTOR3(0, 0.93f, -39));

	CurtainFleam* pCurtainFleam;
	pCurtainFleam = CreateGameObject<CurtainFleam>(this);
	pCurtainFleam->SetPosition(D3DXVECTOR3(0, 1, -39));

	//Calendar* pCalendar;
	//pCalendar = CreateGameObject<Calendar>(this);
	//pCalendar->SetPosition(D3DXVECTOR3(0, 0, 0));

	Toilet* pToilet;
	pToilet = CreateGameObject<Toilet>(this);
	pToilet->SetPosition(D3DXVECTOR3(6.5f, 0.7f, 35));
	pToilet->SetRotateY(90.0f);
	pToilet->SetScale(2.5f, 1.5f, 2.5f);
}


//更新
void Stage::Update()
{	
	//隠し終わったか
	if (nullptr == FindObject("hide_person") && gimmick_turn == false)
	{
		//ギミックターンにするflag
		gimmick_turn = true;
		Gimmick* pGimmick = CreateGameObject<Gimmick>(this);
	}
	
	//ギミックを設置し終わって、探すターンになったか
	if((stage_Gimmick == true) && (find_turn == false) )
	{
		//探すターンにするflag
		find_turn = true;

		/*if(Gimmick_Turn)
		hPict_[0] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[0] >= 0);
		hPict_[1] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[1] >= 0);
		hPict_[2] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[2] >= 0);
		hPict_[3] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[3] >= 0);
		hPict_[4] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[4] >= 0);
		hPict_[5] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[5] >= 0);
		hPict_[6] = Image::Load("Data/Logo/Gimmick.png");
		assert(hPict_[6] >= 0);
		
		
		*/
		pFindingPlayer = CreateGameObject<finding_person>(this);
		pFindingPlayer->SetPosition(D3DXVECTOR3(-37, 1, -37));

	}

		finding_Gimmick();
}

void Stage::finding_Gimmick()
{
	//find_turnの時
	if (find_turn == true)
	{
		//ポジション受け取るやつ
		//D3DXVECTOR3 pos_ = ((Stage*)FindChildObject("finding_person"))->GetPosition();

		 /*pos_ = ((Stage*)SceneManager::GetCurrentScene()->
			FindChildObject("finding_person"))->GetPosition();*/

		pos_ = pFindingPlayer->GetPosition();
	
		/*Map1のslowのフラグの時 &&*/
		if (flag_Gimmic[0] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[0] = true;
			pFindingPlayer->SetDebuff_Slow(0.7);
		}

		//if(Map2時間を減らすフラグの時 && pos.x > xx && pos.y~~~~）
		if(flag_Gimmic[1] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[1] = true;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(2);
		}
/*		//if(Map3時間を減らすフラグの時 && pos.x > xx && pos.y~~~~）
		if (flag_Gimmic[2] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[2] = true;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(2);
		}
		//if(Map4時間を減らすフラグの時 && pos.x > xx && pos.y~~~~）
		if (flag_Gimmic[3] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[3] = true;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(2);
		}
		//if(Map5時間を減らすフラグの時 && pos.x > xx && pos.y~~~~）
		if (flag_Gimmic[4] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[4] = true;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(2);
		}
		//if(Map6時間を減らすフラグの時 && pos.x > xx && pos.y~~~~）
		if (flag_Gimmic[5] == false && pos_.x < 40 && pos_.x > 20)
		{
			flag_Gimmic[5] = true;
			((PlayScene*)SceneManager::GetCurrentScene())->SetTime_tens(2);
		}*/
	}
}

//描画
void Stage::Draw()
{
	/* MAP表示するときに使う
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 画像サイズ), (g.ScHeight), 0);

	Image::SetMatrix(hPict_[0], _worldMatrix);
	Image::Draw(hPict_[0]);

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 画像サイズ), (g.ScHeight), 0);
	Image::SetMatrix(hPict_[1], _worldMatrix);
	Image::Draw(hPict_[1]);

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 画像サイズ), (g.ScHeight), 0);
	Image::SetMatrix(hPict_[2], _worldMatrix);
	Image::Draw(hPict_[2]);

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 画像サイズ), (g.ScHeight), 0);
	Image::SetMatrix(hPict_[3], _worldMatrix);
	Image::Draw(hPict_[3]);

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 画像サイズ), (g.ScHeight), 0);
	Image::SetMatrix(hPict_[4], _worldMatrix);
	Image::Draw(hPict_[4]);

	*/
}

//開放
void Stage::Release()
{
}