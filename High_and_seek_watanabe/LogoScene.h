#pragma once
#include "Engine/global.h"
#include <time.h>

class LogoScene : public IGameObject
{
	int hPict_;
	int fade_In;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測

	int sec_;
public:
	LogoScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};
