#include "CurtainFleam.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
CurtainFleam::CurtainFleam(IGameObject * parent)
	:IGameObject(parent, "CurtainFleam"), hModel_(-1)
{
}

//デストラクタ
CurtainFleam::~CurtainFleam()
{
}

//初期化
void CurtainFleam::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/CurtainFleam.fbx");
	assert(hModel_ >= 0);

}

//更新
void CurtainFleam::Update()
{
}

//描画
void CurtainFleam::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void CurtainFleam::Release()
{
}