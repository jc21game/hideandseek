#pragma once
#include "Engine/ResouceManager/Movie.h"
#include "Engine/Global.h"

class SplashScene : public IGameObject
{
	int countMovieTime_;
	const int MOVIE_TIME = 42000;
	//60フレームの場合 60x1
public:
	SplashScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};
