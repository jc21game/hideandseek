#include "LaundryBasket.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
LaundryBasket::LaundryBasket(IGameObject * parent)
	:IGameObject(parent, "LaundryBasket"), hModel_(-1)
{
}

//デストラクタ
LaundryBasket::~LaundryBasket()
{
}

//初期化
void LaundryBasket::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/LaundryBasket.fbx");
	assert(hModel_ >= 0);


}

//更新
void LaundryBasket::Update()
{
}

//描画
void LaundryBasket::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void LaundryBasket::Release()
{
}