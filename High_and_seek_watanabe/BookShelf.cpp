#include "BookShelf.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
BookShelf::BookShelf(IGameObject * parent)
	:IGameObject(parent, "BookShelf"), hModel_(-1)
{
}

//デストラクタ
BookShelf::~BookShelf()
{
}

//初期化
void BookShelf::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/BookShelf.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 2, 0), D3DXVECTOR3(4, 6, 2));
	AddCollider(collision);
}

//更新
void BookShelf::Update()
{
}

//描画
void BookShelf::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void BookShelf::Release()
{
}