#include "diningtable.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
diningtable::diningtable(IGameObject * parent)
	:IGameObject(parent, "diningtable"), hModel_(-1)
{
}

//デストラクタ
diningtable::~diningtable()
{
}

//初期化
void diningtable::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/diningtable.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 3, 0), D3DXVECTOR3(8, 6, 8));
	AddCollider(collision);

}

//更新
void diningtable::Update()
{
}

//描画
void diningtable::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void diningtable::Release()
{
}