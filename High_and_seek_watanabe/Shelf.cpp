#include "Shelf.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Shelf::Shelf(IGameObject * parent)
	:IGameObject(parent, "Shelf"), hModel_(-1)
{
}

//デストラクタ
Shelf::~Shelf()
{
}

//初期化
void Shelf::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/shelf.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 2, 1));
	AddCollider(collision);

}

//更新
void Shelf::Update()
{
}

//描画
void Shelf::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Shelf::Release()
{
}