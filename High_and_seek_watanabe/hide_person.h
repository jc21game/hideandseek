#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/gameObject/Camera.h"

//hide_personを管理するクラス
class hide_person : public IGameObject
{
	int hModel_;    //モデル番号
	int hPict_[7];    //画像番号
	void move(D3DXVECTOR3 &stick);
	//カメラの回転
	void CamMove(D3DXVECTOR3 &stick);

private:
	float Dash = 1;
	D3DXVECTOR3 prePos;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測
	int sec_;  //1の位
	int searchStatus;

	bool search_result;
	int result_sec;

	bool UI_time[3];
	bool flag_pBed;
	bool flag_cBed;
	bool flag_fridge;
	bool flag_BookShelf;


public:
	//コンストラクタ
	hide_person(IGameObject* parent);

	//デストラクタ
	~hide_person();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void hide_person_move();
	void HitTest();

	D3DXVECTOR3 GetPosition() { return position_; }

	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
	void Search_hide();
	void Search_ParentBed(char status);
	void Search_ChildBed(char status);
	void Search_Fridge(char status);
	void Search_BookShelf(char status);

	void SetSec(int sec) { sec_ += sec; }
	int GetSec() { return sec_; }

	void SetStatus(int stat) { searchStatus = stat; };
	int  GetStatus() { return searchStatus; };
};