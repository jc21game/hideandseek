#include "foliage_plant.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
foliage_plant::foliage_plant(IGameObject * parent)
	:IGameObject(parent, "foliage_plant"), hModel_(-1)
{
}

//デストラクタ
foliage_plant::~foliage_plant()
{
}

//初期化
void foliage_plant::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/FoliagePlant.fbx");
	assert(hModel_ >= 0);
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 1, 0), D3DXVECTOR3(3, 4, 3));
	AddCollider(collision);

}

//更新
void foliage_plant::Update()
{
}

//描画
void foliage_plant::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void foliage_plant::Release()
{
}