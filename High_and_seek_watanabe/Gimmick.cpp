#include "Gimmick.h"
#include "Engine/ResouceManager/Image.h"
#include "Stage.h"
#include "Cursor.h"
#include "BaseWindow.h"
#include "Number.h"
#include "Door_Red.h"
#include "Door_Red2.h"





//コンストラクタ
Gimmick::Gimmick(IGameObject * parent)
	:IGameObject(parent, "Gimmick"), hPict_(-1)
{
}

//デストラクタ
Gimmick::~Gimmick()
{
}

//初期化
void Gimmick::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/DoorGimmickTurn/MAP.png");
	assert(hPict_ >= 0);

	CreateGameObject<Cursor>(this);

	CreateGameObject<BaseWindow>(this);

}

//更新
void Gimmick::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		Stage* pStage = (Stage*)FindObject("Stage");
		pStage->Stage::SetGimmick();
		KillMe();
	}

	if (Input::IsKeyDown(DIK_1))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red>(this);
	}

	if (Input::IsKeyDown(DIK_2))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red2>(this);
	}

	if (Input::IsKeyDown(DIK_3))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red2>(this);
	}

	if (Input::IsKeyDown(DIK_4))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red2>(this);
	}

	if (Input::IsKeyDown(DIK_5))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red>(this);
	}

	if (Input::IsKeyDown(DIK_6))
	{
		CreateGameObject <Number>(this);
		CreateGameObject<Door_Red>(this);
	}

}

//描画
void Gimmick::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void Gimmick::Release()
{
}