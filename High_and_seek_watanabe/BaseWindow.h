#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class BaseWindow : public IGameObject
{

	int hPict_;

public:
	//コンストラクタ
	BaseWindow(IGameObject* parent);

	//デストラクタ
	~BaseWindow();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
