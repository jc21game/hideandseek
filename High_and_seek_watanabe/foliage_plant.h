#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class foliage_plant : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	foliage_plant(IGameObject* parent);

	//デストラクタ
	~foliage_plant();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//int GetModelHandle() { return hModel_; }
};