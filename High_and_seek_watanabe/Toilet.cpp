#include "Toilet.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Toilet::Toilet(IGameObject * parent)
	:IGameObject(parent, "Toilet"), hModel_(-1)
{
}

//デストラクタ
Toilet::~Toilet()
{
}

//初期化
void Toilet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/Toilet.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 2, -1.2f), D3DXVECTOR3(5, 4, 6));
	AddCollider(collision);

}

//更新
void Toilet::Update()
{
}

//描画
void Toilet::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Toilet::Release()
{
}