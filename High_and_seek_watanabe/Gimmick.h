#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/DirectX/Text.h"

//◆◆◆を管理するクラス
class Gimmick : public IGameObject
{
	int hPict_;

	int count;

	bool Gimmick_Set;

	Text* pMathText_;    //数字テキスト


public:
	//コンストラクタ
	Gimmick(IGameObject* parent);

	//デストラクタ
	~Gimmick();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};