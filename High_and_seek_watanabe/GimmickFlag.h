#pragma once
#include "Engine/GameObject/GameObject.h"

//GimmickFlagを管理するクラス
class GimmickFlag : public IGameObject
{
	int _hPict;

	bool Flag_count;

public:
	//コンストラクタ
	GimmickFlag(IGameObject* parent);

	//デストラクタ
	~GimmickFlag();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ギミック発動のフラッグ
	void Flag();


};

