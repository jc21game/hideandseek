#include "Wall.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall"), hModel_(-1)
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/material/WallMalia.fbx");
	assert(hModel_ >= 0);
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Wall::Release()
{
}