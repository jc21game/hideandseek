#include "Entry.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Entry::Entry(IGameObject * parent)
	:IGameObject(parent, "Entry"), hPict_(-1)
{
}

//デストラクタ
Entry::~Entry()
{
}

//初期化
void Entry::Initialize()
{
	hPict_ = Image::Load("data/DoorGimmickTurn/Entry.png");
	assert(hPict_ >= 0);

	position_ = D3DXVECTOR3(848.0f, 560.0f, 0);
}

//更新
void Entry::Update()
{
}

//描画
void Entry::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void Entry::Release()
{
}

