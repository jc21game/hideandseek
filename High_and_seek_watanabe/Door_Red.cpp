#include "Door_Red.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Door_Red::Door_Red(IGameObject * parent)
	:IGameObject(parent, "Door_Red"), hPict_(-1), flag(false)
{
}

//デストラクタ
Door_Red::~Door_Red()
{
}

//初期化
void Door_Red::Initialize()
{
	
	//画像データのロード
	hPict_ = Image::Load("data/DoorGimmickTurn/Door_Red.png");
	assert(hPict_ >= 0);

	position_ = D3DXVECTOR3(303.0f, 240.0f, 0);
}

//更新
void Door_Red::Update()
{
	if (Input::IsKey(DIK_1))
	{
		flag = true;
		position_ = D3DXVECTOR3(304.0f, 240.0f, 0);
	}

	if (Input::IsKey(DIK_5))
	{
		flag = true;
		position_ = D3DXVECTOR3(720.0f, 752.0f, 0);
	}

	if (Input::IsKey(DIK_6))
	{
		flag = true;
		position_ = D3DXVECTOR3(720.0f, 143.0f, 0);
	}
}

//描画
void Door_Red::Draw()
{
    Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void Door_Red::Release()
{
}




