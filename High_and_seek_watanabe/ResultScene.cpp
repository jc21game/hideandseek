#include <assert.h>
#include "ResultScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"),pText_(nullptr), pMathText_(nullptr)
{
}

//初期化
void ResultScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
	pMathText_ = new Text("ＭＳ ゴシック", 64);

	//画像データのロード
	hPict_[0] = Image::Load("Data/Image/KidsWin.png");
	assert(hPict_[0] >= 0);

	hPict_[1] = Image::Load("Data/Image/KidsLose.png");
	assert(hPict_[1] >= 0);

	str.append("クリア時間:");
	str.append(number_[g.timeRecord_tens]);
	str.append(number_[g.timeRecord_sec]);


}

//更新
void ResultScene::Update()
{
	//スペースが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//プレイシーンへ移行
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

//	if (fade_In != 255) fade_In++;

}

//描画
void ResultScene::Draw()
{
	if(g.winFlag_)
	{
		Image::SetMatrix(hPict_[0], _worldMatrix);
		Image::Draw(hPict_[0]);
		pMathText_->Draw(g.ScWidth / 2 - 200, (g.ScHeight / 2) - 300, str);
	}
	else
	{
		Image::SetMatrix(hPict_[1], _worldMatrix);
		Image::Draw(hPict_[1]);
	}
}

//開放
void ResultScene::Release()
{
}