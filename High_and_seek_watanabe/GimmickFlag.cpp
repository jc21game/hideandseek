#include "GimmickFlag.h"
#include "Engine/ResouceManager/Image.h"
#include "Stage.h"
#include "hide_person.h"

//コンストラクタ
GimmickFlag::GimmickFlag(IGameObject * parent)
	:IGameObject(parent, "GimmickFlag"), _hPict(-1), Flag_count(false)
{
}

//デストラクタ
GimmickFlag::~GimmickFlag()
{
}

//初期化
void GimmickFlag::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Map.png");
	assert(_hPict >= 0);
}

//更新
void GimmickFlag::Update()
{
}

//描画
void GimmickFlag::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void GimmickFlag::Release()
{
}

//Flag発動条件
void GimmickFlag::Flag()
{
	hide_person* hide;
	D3DXVECTOR3 pos = ((Stage*)SceneManager::GetCurrentScene()->FindChildObject("hide_person"))->GetPosition();

	if (pos.x < 30 && pos.x > 32 )
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
	
}


