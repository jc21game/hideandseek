#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Door_Red : public IGameObject
{
	int hPict_;

	bool flag;

public:
	//コンストラクタ
	Door_Red(IGameObject* parent);

	//デストラクタ
	~Door_Red();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

