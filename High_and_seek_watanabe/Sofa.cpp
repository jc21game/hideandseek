#include "Sofa.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Sofa::Sofa(IGameObject * parent)
	:IGameObject(parent, "Sofa"), hModel_(-1)
{
}

//デストラクタ
Sofa::~Sofa()
{
}

//初期化
void Sofa::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/Sofa.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 2, 0), D3DXVECTOR3(10, 4, 5));
	AddCollider(collision);

	SetScale(2, 2, 2);
}

//更新
void Sofa::Update()
{
}

//描画
void Sofa::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Sofa::Release()
{
}