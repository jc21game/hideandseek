#include "fridge.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
fridge::fridge(IGameObject * parent)
	:IGameObject(parent, "fridge"), hModel_(-1)
{
}

//デストラクタ
fridge::~fridge()
{
}

//初期化
void fridge::Initialize()
{
	//モデルデータのロード
	//hModel_ = Model::Load("data/Model/furniture/Fridge.fbx");
	//assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 3, 0), D3DXVECTOR3(3, 5, 3));
	AddCollider(collision);
	SetScale(2, 2, 2);

}

//更新
void fridge::Update()
{
}

//描画
void fridge::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void fridge::Release()
{
}