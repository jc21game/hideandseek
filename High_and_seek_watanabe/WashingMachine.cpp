#include "WashingMachine.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
WashingMachine::WashingMachine(IGameObject * parent)
	:IGameObject(parent, "WashingMachine"), hModel_(-1)
{
}

//デストラクタ
WashingMachine::~WashingMachine()
{
}

//初期化
void WashingMachine::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/WashingMachine.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(3.5f, 3.5f, 3.5f));
	AddCollider(collision);

}

//更新
void WashingMachine::Update()
{
}

//描画
void WashingMachine::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void WashingMachine::Release()
{
}