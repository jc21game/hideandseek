#include "Number.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/global.h"
#include "Gimmick.h"


//コンストラクタ
Number::Number(IGameObject * parent)
	:IGameObject(parent, "Number"), hPict_()
{
}

//デストラクタ
Number::~Number()
{
}

//初期化
void Number::Initialize()
{
	////1番
	//hPict_[1] = Image::Load("data/DoorGimmickTurn/Number_1.png");
	//assert(hPict_[1] >= 0);
	////2番
	//hPict_[2] = Image::Load("data/DoorGimmickTurn/Number_2.png");
	//assert(hPict_[2] >= 0);
	////3番
	//hPict_[3] = Image::Load("data/DoorGimmickTurn/Number_3.png");
	//assert(hPict_[3] >= 0);
	////4番
	//hPict_[4] = Image::Load("data/DoorGimmickTurn/Number_4.png");
	//assert(hPict_[4] >= 0);
	////5番
	//hPict_[5] = Image::Load("data/DoorGimmickTurn/Number_5.png");
	//assert(hPict_[5] >= 0);
	////6番
	//hPict_[6] = Image::Load("data/DoorGimmickTurn/Number_6.png");
	//assert(hPict_[6] >= 0);

	std::string fileName[] =
	{
		"data/DoorGimmickTurn/Number_1.png",
		"data/DoorGimmickTurn/Number_2.png",
		"data/DoorGimmickTurn/Number_3.png",
		"data/DoorGimmickTurn/Number_4.png",
		"data/DoorGimmickTurn/Number_5.png",
		"data/DoorGimmickTurn/Number_6.png"
	};

	for (int i = 0; i < 6; i++)
	{
		hPict_[i] = Image::Load(fileName[i]);
		assert(hPict_[i] > -1);
	}
}

//更新
void Number::Update()
{
	if (Input::IsKeyDown(DIK_1))
	{
		hPict_[0];
	}

	if (Input::IsKeyDown(DIK_2))
	{
		hPict_[1];
		
	}

	if (Input::IsKeyDown(DIK_3))
	{
		hPict_[2];
	}

	if (Input::IsKeyDown(DIK_4))
	{
		hPict_[3];
	}

	if (Input::IsKeyDown(DIK_5))
	{
		hPict_[4];
	}

	if (Input::IsKeyDown(DIK_6))
	{
		hPict_[5];
	}


}

//描画
void Number::Draw()
{
	D3DXMATRIX	m;
	D3DXMatrixTranslation(&m, (g.ScWidth - 500), (g.ScHeight - 500), 0);

	Image::SetMatrix(hPict_[0], m);
	Image::Draw(hPict_[0]);


	Image::SetMatrix(hPict_[1], m);
	Image::Draw(hPict_[1]);
	
}

//開放
void Number::Release()
{
}

