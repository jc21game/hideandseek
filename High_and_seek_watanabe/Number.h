#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Number : public IGameObject
{


	int hPict_[7];

public:
	//コンストラクタ
	Number(IGameObject* parent);

	//デストラクタ
	~Number();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

