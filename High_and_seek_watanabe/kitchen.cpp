#include "Kitchen.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Kitchen::Kitchen(IGameObject * parent)
	:IGameObject(parent, "Kitchen"), hModel_(-1)
{
}

//デストラクタ
Kitchen::~Kitchen()
{
}

//初期化
void Kitchen::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/kichen.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 3, 0), D3DXVECTOR3(11, 7, 6));
	AddCollider(collision);

}

//更新
void Kitchen::Update()
{
}

//描画
void Kitchen::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Kitchen::Release()
{
}