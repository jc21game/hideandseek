#include "longTrashCan.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
longTrashCan::longTrashCan(IGameObject * parent)
	:IGameObject(parent, "longTrashCan"), hModel_(-1)
{
}

//デストラクタ
longTrashCan::~longTrashCan()
{
}

//初期化
void longTrashCan::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/LongTrashCan.fbx");
	assert(hModel_ >= 0);


}

//更新
void longTrashCan::Update()
{
}

//描画
void longTrashCan::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void longTrashCan::Release()
{
}