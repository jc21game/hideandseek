#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/gameObject/Camera.h"

//finding_personを管理するクラス
class finding_person : public IGameObject
{
	int hModel_;    //モデル番号
	int hPict_[2];    //画像番号
	void move(D3DXVECTOR3 &stick);
	//カメラの回転
	void CamMove(D3DXVECTOR3 &stick);

private:
	float Debuff = 1;
	D3DXVECTOR3 prePos;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測
	int find_sec;  //1の位
	int result_sec;
	int searchStatus;

	bool search_result;
	bool flag_pBed;
	bool flag_cBed;
	bool flag_fridge;
	bool flag_BookShelf;

	int Flag_time = 0;

public:
	//コンストラクタ
	finding_person(IGameObject* parent);

	//デストラクタ
	~finding_person();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void finding_person_move();
	void HitTest();


	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
	void Search_finding();
	void Search_ParentBed();
	void Search_ChildBed();
	void Search_Fridge();
	void Search_BookShelf();

	void SetDebuff_Slow(float Debuff_) { Debuff = Debuff_; }
};