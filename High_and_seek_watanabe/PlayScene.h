#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"
//時間計測用
#include "Engine/global.h"
#include <string>
#include <time.h>

#define MIN 0
#define TENS 6
#define SEC 0

#define FIND_TENS 6

class PlayScene : public IGameObject
{
	Text* pText_;    //テキスト

	Text* pMathText_;    //数字テキスト

	//表示する文字列の配列
	std::string number_[10] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
	//表示する文字列の格納
	std::string str;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測

	struct timer
	{
		int min_;  //100の位
		int tens_; //10の位
		int sec_;  //1の位
	}timer_;
	
	bool resetTime_;
	bool findMessage_;

public:
	PlayScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
	void SetFindMessage(bool find) { findMessage_ = find; }

	//時間をカウントする関数
	void CountTimer();
	// -= なので、正の値で入れればよい
	void SetTime_tens(int tens) { timer_.tens_ -= tens; }
	int GetTime_tens() { return timer_.tens_; }
	int GetTime_sec() { return timer_.sec_; }
};
