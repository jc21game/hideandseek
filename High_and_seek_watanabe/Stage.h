#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Player.h"
#include "hide_person.h"
#include "finding_person.h"

//◆◆◆を管理するクラス
class Stage : public IGameObject
{
	//D3DXVECTOR3 Pos;
	//Player* pPlayer_;

private:
	//罠を仕掛けるターンになったかどうか
	bool gimmick_turn;
	//ギミックがONになったか
	bool stage_Gimmick;
	//探すターンになったか
	bool find_turn;

	hide_person* pHidePlayer;
	finding_person* pFindingPlayer;
	D3DXVECTOR3 pos_;

	bool flag_Gimmic[7];

	int hPict_[7];

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void finding_Gimmick();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void StageObject();

	void furniture();

	void ActionPoint();

	void SetGimmick() { stage_Gimmick = true; }

	void SetFlag_Gimmick(int number) { flag_Gimmic[number] = true; }

};