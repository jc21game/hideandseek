#include "TrashCan.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
TrashCan::TrashCan(IGameObject * parent)
	:IGameObject(parent, "TrashCan"), hModel_(-1)
{
}

//デストラクタ
TrashCan::~TrashCan()
{
}

//初期化
void TrashCan::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/Trash Can.fbx");
	assert(hModel_ >= 0);
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 1, 0), D3DXVECTOR3(2, 2, 2));
	AddCollider(collision);

}

//更新
void TrashCan::Update()
{
}

//描画
void TrashCan::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void TrashCan::Release()
{
}