﻿#include "PlayScene.h"
#include "Engine/global.h"
#include "Player.h"
#include "Stage.h"
#include "Engine/gameObject/Camera.h"

PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), pText_(nullptr), pMathText_(nullptr)
{
	timer_.min_ = MIN;
	timer_.tens_ = TENS;
	timer_.sec_ = SEC;
}

void PlayScene::Initialize()
{
	CreateGameObject<Stage>(this);
	//CreateGameObject<Player>(this);
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
	pMathText_ = new Text("ＭＳ ゴシック", 64);

	//現在時刻(秒)
	time(&startTime_);

	//文字列の結合(min_ + tens_ + sec_ = str)
	str.append(number_[timer_.min_]);
	str.append(number_[timer_.tens_]);
	str.append(number_[timer_.sec_]);
}
 
void PlayScene::Update()
{
	//時間を計測する関数
	if (FindObject("finding_person")) { CountTimer(); }

	//探す人がいる＝ターンが変わる
	//ターンが変わる＝タイムがリセットされる。
	IGameObject* pfindPlayer;
	pfindPlayer = FindChildObject("finding_person");

	//タイムをリセットしたら、flagを立てて2回目は実行しないようにする。
	if (pfindPlayer != nullptr && (resetTime_ == false))
	{
		resetTime_ = true;
		timer_.min_ = 0;
		timer_.tens_ = FIND_TENS;
		timer_.sec_ = 0;
	}
}

///////////時間を計測///////////
void PlayScene::CountTimer()
{
	//経過時間
	limitTime_ = time(&limitTime_);

	//経過時間が１秒以上なら
	if ((limitTime_ - startTime_) >= 1)
	{
		//開始時間を計測
		startTime_ = limitTime_;

		//文字列を削除
		str.clear();

		//一の位が０になったら
		if (timer_.sec_ <= 0)
		{
			//十の位が０だったら
			if (timer_.tens_ == 0)
			{
				timer_.min_--;     //百の位をデクリメント
				timer_.tens_ = 9;  //十の位を９にセット
			}
			//十の位が0じゃなかったら
			else
			{
				timer_.tens_--;    //十の位をデクリメント
			}

			timer_.sec_ = 9;       //一の位を９にセット
		}
		//一の位が０じゃなかったら
		else if (timer_.sec_ != 0)
		{
			timer_.sec_--;         //一の位をデクリメント
		}

		//それぞれの値の文字列を格納
		str.append(number_[timer_.min_]);
		str.append(number_[timer_.tens_]);
		str.append(number_[timer_.sec_]);
	}

	//全ての位が０になったら
	if (timer_.min_ == 0 && timer_.tens_ == 0 && timer_.sec_ <= 0)
	{
		//リザルトシーンへ移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
		g.timeRecord_tens = 0;
		g.timeRecord_sec = 0;
		g.winFlag_ = false;
	}
}

void PlayScene::Draw()
{
	if (FindObject("finding_person")) { pMathText_->Draw(960, 100, str); }

	if (findMessage_ == true) { pText_->Draw(300, 300, "宝物は見つかりませんでした。"); }
}

void PlayScene::Release()
{
	delete pText_;
	delete pMathText_;
}
