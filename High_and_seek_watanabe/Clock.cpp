#include "Clock.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Clock::Clock(IGameObject * parent)
	:IGameObject(parent, "Clock"), hModel_(-1)
{
}

//デストラクタ
Clock::~Clock()
{
}

//初期化
void Clock::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/Clock.fbx");
	assert(hModel_ >= 0);

	SetScale(2, 2, 1);
}

//更新
void Clock::Update()
{
}

//描画
void Clock::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Clock::Release()
{
}