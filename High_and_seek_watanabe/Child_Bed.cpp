#include "Child_Bed.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Child_Bed::Child_Bed(IGameObject * parent)
	:IGameObject(parent, "Child_Bed"), hModel_(-1)
{
}

//デストラクタ
Child_Bed::~Child_Bed()
{
}

//初期化
void Child_Bed::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/Bed.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 1, 0), D3DXVECTOR3(6, 2, 12));
	AddCollider(collision);
}

//更新
void Child_Bed::Update()
{
}

//描画
void Child_Bed::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Child_Bed::Release()
{
}