#include "SplashScene.h"

SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene")
{
}

void SplashScene::Initialize()
{
	//モデルデータのロード
	Movie::Load(L"data/Logo/teamrogo.avi");
	Movie::Play();

	g.movieFlag_ = true;

	//Direct3Dを止める
	//Sleep(Movie::GetMovieLength());
	
}

void SplashScene::Update()
{
	if (countMovieTime_ > MOVIE_TIME || Input::IsKey(DIK_SPACE))
	{
		//動画の再生終わり　シーン切り替え
		g.movieFlag_ = false;
		Movie::Stop();
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_LOGO);
	}
	countMovieTime_++;
}

void SplashScene::Draw()
{	
}

void SplashScene::Release()
{
}
