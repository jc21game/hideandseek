#pragma onceResultScene

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"
#include <string>

//タイトルシーンを管理するクラス
class ResultScene : public IGameObject
{
	Text* pText_;    //テキスト
	int hPict_[3];    //画像番号
	Text* pMathText_;    //数字テキスト

	//表示する文字列の配列
	std::string number_[10] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
	//表示する文字列の格納
	std::string str;

	struct timer
	{
		int min_;  //100の位
		int tens_; //10の位
		int sec_;  //1の位
	}timer_;

public:
	//コンストラクタ
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};