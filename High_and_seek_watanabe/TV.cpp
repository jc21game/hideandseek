#include "TV.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
TV::TV(IGameObject * parent)
	:IGameObject(parent, "TV"), hModel_(-1)
{
}

//デストラクタ
TV::~TV()
{
}

//初期化
void TV::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/TV.fbx");
	assert(hModel_ >= 0);
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 2, 0), D3DXVECTOR3(10, 4, 7));
	AddCollider(collision);

}

//更新
void TV::Update()
{
}

//描画
void TV::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void TV::Release()
{
}