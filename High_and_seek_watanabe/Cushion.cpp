#include "Cushion.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Cushion::Cushion(IGameObject * parent)
	:IGameObject(parent, "Cushion"), hModel_(-1)
{
}

//デストラクタ
Cushion::~Cushion()
{
}

//初期化
void Cushion::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/interior/Cushion.fbx");
	assert(hModel_ >= 0);


}

//更新
void Cushion::Update()
{
}

//描画
void Cushion::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Cushion::Release()
{
}