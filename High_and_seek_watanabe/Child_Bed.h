#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Child_Bed : public IGameObject
{
	int hModel_;
	int status;
public:
	//コンストラクタ
	Child_Bed(IGameObject* parent);

	//デストラクタ
	~Child_Bed();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//物のステータス
	void SetStatus(int change) { status = change; }
	int GetStatus() { return status; };
};