#include "Ceiling.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Ceiling::Ceiling(IGameObject * parent)
	:IGameObject(parent, "Ceiling"), hModel_(-1)
{
}

//デストラクタ
Ceiling::~Ceiling()
{
}

//初期化
void Ceiling::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/material/Ceiling.fbx");
	assert(hModel_ >= 0);
}

//更新
void Ceiling::Update()
{
}

//描画
void Ceiling::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Ceiling::Release()
{
}