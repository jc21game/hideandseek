#include "SplashScene.h"
#include "Engine/global.h"
#include "LogoScene.h"
#include "Engine/ResouceManager/Image.h"

LogoScene::LogoScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), hPict_(-1), fade_In(0), sec_(5)
{
}

void LogoScene::Initialize()
{
	//画像ロード
	hPict_ = Image::Load("data/Logo/touhokudenshi.png");
	assert(hPict_ >= 0);

	time(&startTime_);
}

void LogoScene::Update()
{
	//経過時間
	limitTime_ = time(&limitTime_);
	if(fade_In != 255) fade_In++;

	//経過時間が１秒以上なら
	if ((limitTime_ - startTime_) >= 1)
	{
		//開始時間を計測
		startTime_ = limitTime_;
		if (sec_ != 0)
		{
			sec_--;
		}
	}

	if (Input::IsKeyDown(DIK_SPACE) || sec_ == 0)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

void LogoScene::Draw()
{
	//画像出すとき
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (g.ScWidth / 2) - 512, (g.ScHeight / 2) - (576 / 2), 0);
		
	Image::SetMatrix(hPict_, m);
	Image::AlphaDraw(hPict_, fade_In);
}

void LogoScene::Release()
{
}
