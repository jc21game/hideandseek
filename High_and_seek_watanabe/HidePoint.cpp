#include "HidePoint.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
HidePoint::HidePoint(IGameObject * parent)
	:IGameObject(parent, "HidePoint"), hModel_(-1)
{
}

//デストラクタ
HidePoint::~HidePoint()

{
}

//初期化
void HidePoint::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/HidePoint.fbx");
	assert(hModel_ >= 0);

	/*BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 1, 0), D3DXVECTOR3(3, 2, 3));
	AddCollider(collision);*/
}

//更新
void HidePoint::Update()
{
}

//描画
void HidePoint::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void HidePoint::Release()
{
}