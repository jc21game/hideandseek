#include "hide_person.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"
#include "XINPUT.h"
#include "Wall.h"

#include <time.h>
#include "furniture.h"
#include "Stage.h"

//コンストラクタ
hide_person::hide_person(IGameObject * parent)
	:IGameObject(parent, "hide_person"), hModel_(-1), sec_(0), searchStatus(0), result_sec(0)
{
	for (int i = 0; i < 7; i++)
	{
		hPict_[i] = -1;
	}
	UI_time[0] = false;
	UI_time[1] = false;
	UI_time[2] = false;
}

//デストラクタ
hide_person::~hide_person()
{
}

//初期化
void hide_person::Initialize()
{
	//モデルデータのロード
	/*hModel_ = Model::Load("data/Model/player.fbx");
	assert(hModel_ >= 0);*/

	//画像データのロード
	hPict_[0] = Image::Load("Data/Hide_things.png");
	assert(hPict_[0] >= 0);

	hPict_[1] = Image::Load("Data/Hide_Trap.png");
	assert(hPict_[1] >= 0);

	//アクションキー(1,2,3)の画像のロード
	hPict_[2] = Image::Load("Data/PlaySceneUI/BaseUI_under.png");
	assert(hPict_[2] >= 0);

	hPict_[3] = Image::Load("Data/PlaySceneUI/BaseUI_upper.png");
	assert(hPict_[3] >= 0);

	hPict_[4] = Image::Load("Data/PlaySceneUI/Icon_clock.png");
	assert(hPict_[4] >= 0);

	hPict_[5] = Image::Load("Data/PlaySceneUI/Icon_redClock.png");
	assert(hPict_[5] >= 0);

	hPict_[6] = Image::Load("Data/PlaySceneUI/Icon_candy.png");
	assert(hPict_[6] >= 0);
	//カメラ
	Camera* pCamera_ = CreateGameObject<Camera>(this);
	assert((pCamera_) != nullptr);
	pCamera_->SetPosition(D3DXVECTOR3(0, 4.0f, 0));
	pCamera_->SetTarget(D3DXVECTOR3(0, 0, 10));

	//スポーン地点
	SetPosition(D3DXVECTOR3(-37, 1, -37));

	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, -1, 0), D3DXVECTOR3(1, 3, 1));
	AddCollider(collision);

}

//更新
void hide_person::Update()
{

	//プレイヤーの移動動作
	if (searchStatus != 1)
	{
		hide_person_move();
	}
	//当たり判定の処理
	HitTest();
	Search_hide();

	if (result_sec > 240)
	{
		search_result = false;
		result_sec = 0;
	}
	if (search_result) result_sec++;
}

void hide_person::hide_person_move()
{
	//動く前に直前の位置を取る
	prePos = position_;

	//コントローラ
	D3DXVECTOR3 leftStick = Input::GetPadStickL();
	D3DXVECTOR3 rightStick = Input::GetPadStickR();

	move(leftStick);	//キー、コントローラの処理
	CamMove(rightStick);

	//現在の位置 - 前にいた位置でベクトルの角度を取る
	//現在と前の位置の「差」
	D3DXVECTOR3 move = position_;// - prePos;

}

void hide_person::move(D3DXVECTOR3 &stick)
{

	//////////////////////////////////////////////////////////////

		//回転行列を用意
	D3DXMATRIX matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(rotate_.y));

	D3DXVECTOR3 vecForward = D3DXVECTOR3(0, 0, 0.3f);			//奥移動ベクトル
	D3DXVec3TransformCoord(&vecForward, &vecForward, &matRot);

	D3DXVECTOR3 vecRight = D3DXVECTOR3(0.3f, 0, 0);				//右移動ベクトル
	D3DXVec3TransformCoord(&vecRight, &vecRight, &matRot);

	////////////////////////////////////////////////////////////////

	//押してる間ダッシュ
	if (Input::IsKey(DIK_E))
	{
		Dash = 2.0f;
	}
	else
	{
		Dash = 1.0f;
	}

	if (position_.x >= -79.0f && position_.x <= 79.0f &&
		position_.z >= -79.0f  && position_.z <= 79.0f &&
		position_.y >= -160.0f && position_.y <= 160.0f)
	{
		if (Input::IsKey(DIK_W))	position_ += vecForward * Dash;
		if (Input::IsKey(DIK_S))	position_ -= vecForward * Dash;
		if (Input::IsKey(DIK_A))	position_ -= vecRight * Dash;
		if (Input::IsKey(DIK_D))	position_ += vecRight * Dash;
		if (Input::IsKey(DIK_SPACE))position_.y += 1.0f;
		if (Input::IsKey(DIK_LSHIFT))position_.y -= 1.0f;

		//移動コントローラー
		if (stick.z > 0)	position_ += vecForward * Dash;
		if (stick.z < 0)	position_ -= vecForward * Dash;
		if (stick.x > 0)	position_ += vecRight * Dash;
		if (stick.x < 0)	position_ -= vecRight * Dash;
		if (Input::IsPadButton(XINPUT_GAMEPAD_A))position_.y += 1.0f;
		if (Input::IsPadButton(XINPUT_GAMEPAD_B))position_.y -= 1.0f;
	}
	else
	{
		if (position_.z <= -79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, -79.0f);
		if (position_.z >= 79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, 79.0f);
		if (position_.x <= -79.0f) 		position_ = D3DXVECTOR3(-79.0f, position_.y, position_.z);
		if (position_.x >= 79.0f)		position_ = D3DXVECTOR3(79.0f, position_.y, position_.z);
	}

}

//カメラ回転
void hide_person::CamMove(D3DXVECTOR3 &stick)
{
	//ゲームパッド
	if (stick.y > 0)		rotate_.x -= 1.0f;
	if (stick.y < 0)		rotate_.x += 1.0f;
	if (stick.x < 0)		rotate_.y -= 1.0f;
	if (stick.x > 0)		rotate_.y += 1.0f;

	//キーボード
	if (Input::IsKey(DIK_UP))		rotate_.x -= 1.0f;
	if (Input::IsKey(DIK_DOWN))		rotate_.x += 1.0f;
	if (Input::IsKey(DIK_LEFT))		rotate_.y -= 1.0f;
	if (Input::IsKey(DIK_RIGHT))	rotate_.y += 1.0f;
}

void hide_person::HitTest()
{
	Wall* pWall = (Wall*)FindObject("Wall");    //ステージオブジェクトを探す
	int hWall = pWall->GetModelHandle();    //モデル番号を取得

	//右
	RayCastData right;
	right.start = position_;              //レイの発射位置
	right.dir = D3DXVECTOR3(1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &right); //レイを発射

	  //レイが当たったら
	if (right.hit)
	{
		if (right.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//左
	RayCastData left;
	left.start = position_;              //レイの発射位置
	left.dir = D3DXVECTOR3(-1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &left); //レイを発射

	  //レイが当たったら
	if (left.hit)
	{
		if (left.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//上	
	RayCastData forward;
	forward.start = position_;              //レイの発射位置
	forward.dir = D3DXVECTOR3(0, 0, 1);   //レイの方向
	Model::RayCast(hWall, &forward); //レイを発射

	  //レイが当たったら
	if (forward.hit)
	{
		if (forward.dist < 1.0f)
		{
			//position_ -= vecForward;
			position_ = prePos;
		}
	}

	//下
	RayCastData backward;
	backward.start = position_;              //レイの発射位置
	backward.dir = D3DXVECTOR3(0, 0, -1);    //レイの方向
	Model::RayCast(hWall, &backward); //レイを発射

	  //レイが当たったら
	if (backward.hit)
	{
		if (backward.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

}


//描画
void hide_person::Draw()
{
	//Model::SetMatrix(hModel_, _worldMatrix);
	//Model::Draw(hModel_);
	if (sec_ > 1 && searchStatus == 1)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (g.ScWidth / 2) - 256, (g.ScHeight / 2) - 64, 0);

		Image::SetMatrix(hPict_[0], m);
		Image::Draw(hPict_[0]);
	}
	if (search_result)	Image::Draw(hPict_[1]);

	D3DXMATRIX Base;
	D3DXMatrixTranslation(&Base, (g.ScWidth - 512), (g.ScHeight - 128), 0);

	Image::SetMatrix(hPict_[2], Base);
	Image::SetMatrix(hPict_[3], Base);

	D3DXMatrixTranslation(&Base, (g.ScWidth - 512) + 88, (g.ScHeight - 128), 0);
	Image::SetMatrix(hPict_[4], Base);

	D3DXMATRIX Base_clock;
	D3DXMatrixTranslation(&Base_clock, (g.ScWidth - 512) + 96 + 96, (g.ScHeight - 128), 0);
	Image::SetMatrix(hPict_[5], Base_clock);

	D3DXMATRIX Base_candy;
	D3DXMatrixTranslation(&Base_candy, (g.ScWidth - 512) + 96 + 96 + 96 + 8, (g.ScHeight - 128), 0);
	Image::SetMatrix(hPict_[6], Base_candy);

	Image::Draw(hPict_[2]);

	if (!UI_time[0])Image::Draw(hPict_[4]);
	if (!UI_time[1])Image::Draw(hPict_[5]);
	if (!UI_time[2])Image::Draw(hPict_[6]);

	//Base_upper
	Image::Draw(hPict_[3]);


}

//開放
void hide_person::Release()
{
}

void hide_person::OnCollision(IGameObject * pTarget)
{
	//HidePointの上にいる時、Eキーで隠す
	//Eキーで物が入ってた場合、"物がある" → "物を取った"(物がないとは別）状態にする
	//物が取られていた場合Resultシーン（勝利）にする。

	position_ = prePos;
}

//隠す側のサーチ
//xは５、zは８
void hide_person::Search_hide()
{
	if (Input::IsKey(DIK_1) || Input::IsKey(DIK_2) ||
		Input::IsKey(DIK_3))
	{
		if (Input::IsKey(DIK_1))
		{
			if (!flag_pBed) Search_ParentBed(Time);
			if (!flag_cBed) Search_ChildBed(Time);
			if (!flag_fridge) Search_Fridge(Time);
			if (!flag_BookShelf) Search_BookShelf(Time);
			if (sec_ > 60) //1秒経ったら、
			{
				UI_time[0] = true;
			}
		}
		if (Input::IsKey(DIK_2))
		{
			if (!flag_pBed) Search_ParentBed(Time);
			if (!flag_cBed) Search_ChildBed(Time);
			if (!flag_fridge) Search_Fridge(Time);
			if (!flag_BookShelf) Search_BookShelf(Time);
			if (sec_ > 60) //1秒経ったら、
			{
				UI_time[1] = true;
			}
		}

		if (Input::IsKey(DIK_3))
		{
			if (!flag_pBed) Search_ParentBed(Have);
			if (!flag_cBed) Search_ChildBed(Have);
			if (!flag_fridge) Search_Fridge(Have);
			if (!flag_BookShelf) Search_BookShelf(Have);
			if (sec_ > 60) //1秒経ったら、
			{
				UI_time[2] = true;
			}
		}
	}
	else
	{
		sec_ = 0;
		searchStatus = 0;
	}

}

void hide_person::Search_ParentBed(char status)
{
	if (position_.x >= 30 && position_.x <= 35 &&
		position_.z <= 30 && position_.z >= 23)
	{
		if (sec_ > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			Parent_Bed* pParentBed = (Parent_Bed*)FindObject("Parent_Bed");
			pParentBed->SetStatus(status);
			//家具があったら、SetStatusで状態を変える。
			//探す側は家具があったら、GetStatusで見る。
			//物があったらゲームを終了させる。

			//flag
			flag_pBed = true;
			if (status == Have)
			{
				KillMe();
			}
			else
			{
				search_result = true;
			}
		}
		else
		{
			searchStatus = 1;
		}
		sec_++;
	}
}

void hide_person::Search_ChildBed(char status)
{
	if (position_.x >= 30 && position_.x <= 35 &&
		position_.z <= -22 && position_.z >= -30)
	{
		if (sec_ > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			Child_Bed* pChildBed = (Child_Bed*)FindObject("Child_Bed");
			pChildBed->SetStatus(status);
			//家具があったら、SetStatusで状態を変える。
			//探す側は家具があったら、GetStatusで見る。
			//物があったらゲームを終了させる。
			flag_cBed = true;
			if (status == Have)
			{
				KillMe();
			}
			else
			{
				search_result = true;
			}
		}
		else
		{
			searchStatus = 1;
		}
		sec_++;
	}
}

void hide_person::Search_Fridge(char status)
{
	if (position_.x <= -15 && position_.x >= -23 &&
		position_.z <= -15 && position_.z >= -20.5f)
	{
		if (sec_ > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			fridge* pfridge = (fridge*)FindObject("fridge");
			pfridge->SetStatus(status);
			//家具があったら、SetStatusで状態を変える。
			//探す側は家具があったら、GetStatusで見る。
			//物があったらゲームを終了させる。

			//flag
			flag_fridge = true;
			if (status == Have)
			{
				KillMe();
			}
			else
			{
				search_result = true;
			}
		}
		else
		{
			searchStatus = 1;
		}
		sec_++;
	}
}

void hide_person::Search_BookShelf(char status)
{
	if (position_.x >= 34 && position_.x <= 40 &&
		position_.z >= 2 && position_.z <= 9)
	{
		if (sec_ > 60) //1秒経ったら、
		{
			//探索中画像を消す
			searchStatus = 0;
			BookShelf* pBookShelf = (BookShelf*)FindObject("BookShelf");
			pBookShelf->SetStatus(status);
			//家具があったら、SetStatusで状態を変える。
			//探す側は家具があったら、GetStatusで見る。
			//物があったらゲームを終了させる。

			//flag
			flag_BookShelf = true;
			if (status == Have)
			{
				KillMe();
			}
			else
			{
				search_result = true;
			}
		}
		else
		{
			searchStatus = 1;
		}
		sec_++;
	}
}
