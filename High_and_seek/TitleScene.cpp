#include <assert.h>
#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"
#include <time.h>

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), hPict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);

	//画像データのロード
	hPict_ = Image::Load("Data/Logo/title.png"); 
	assert(hPict_ >= 0);
}

//更新
void TitleScene::Update()
{
	//エンターが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//プレイシーンへ移行
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	//スペースが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//プレイシーンへ移行
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
	//pText_->Draw(100, 100, "これはタイトル用のシーンです。SPACEかEnterで次のシーン");
}

//開放
void TitleScene::Release()
{
}