#include "OperationMethodScene.h"
#include "Engine/global.h"

OperationMethodScene::OperationMethodScene(IGameObject * parent)
	: IGameObject(parent, "OperationMethodScene"), pText_(nullptr)
{
}

void OperationMethodScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
}

void OperationMethodScene::Update()
{
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

void OperationMethodScene::Draw()
{
	pText_->Draw(100, 100, "操作方法シーン");
}

void OperationMethodScene::Release()
{
	delete pText_;
}
