#include "Cursor.h"
#include "Engine/ResouceManager/Image.h"
#include "CharacterSelect.h"


//コンストラクタ
Cursor::Cursor(IGameObject * parent)
	:IGameObject(parent, "Cursor"), hPict_(-1)
{
}

//デストラクタ
Cursor::~Cursor()
{
}

//初期化
void Cursor::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/Cursor.png");
	assert(hPict_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(1500.0f, 200.0f, 0);
}

//更新
void Cursor::Update()
{
	//カーソルの移動
	//上に移動制限
	if (position_.y != 800)
	{
		//↓が押されていたら
		if (Input::IsKeyDown(DIK_DOWN))
		{
			position_.y += 200.0f;
			count++;
		}
	}
	//下に移動制限
	if (position_.y != 200)
	{
		//↑が押されていたら
		if (Input::IsKeyDown(DIK_UP))
		{
			position_.y -= 200.0f;
			count--;
		}
	}

	//ENTERで現在選択してるシーンへ
	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	if (Input::IsKeyDown(DIK_RETURN))
	{
		switch (count)
		{
		case 0:
			//プレイシーンへ
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
			break;

		case 1:
			//キャラクター選択へ
			pSceneManager->ChangeScene(SCENE_ID_CHAR);
			break;

		case 2:
			//オプションへ
			pSceneManager->ChangeScene(SCENE_ID_OPTION);
			break;

		case 3:
			//操作方法表示
			pSceneManager->ChangeScene(SCENE_ID_OPERATIONMETHOD);
			break;

		}
	}
}

//描画
void Cursor::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Cursor::Release()
{
}