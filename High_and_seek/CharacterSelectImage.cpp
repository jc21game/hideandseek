#include "CharacterSelectImage.h"
#include "Engine/ResouceManager/Image.h"
#include "MenuScene.h"


//コンストラクタ
CharacterSelectImage::CharacterSelectImage(IGameObject * parent)
	:IGameObject(parent, "CharacterSelectImage"), hPict_(-1)
{
}

//デストラクタ
CharacterSelectImage::~CharacterSelectImage()
{
}

//初期化
void CharacterSelectImage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/CharacterSelectImage.png");
	assert(hPict_ >= 0);
}

//更新
void CharacterSelectImage::Update()
{
	//↓が押されていたら
	if (Input::IsKeyDown(DIK_DOWN))
	{
		count++;
	}
	//↑が押されていたら
	if (Input::IsKeyDown(DIK_UP))
	{
		count++;
	}

	//画像削除
	if (count == 2)
	{
		count = 0;
		KillMe();
	}
}

//描画
void CharacterSelectImage::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void CharacterSelectImage::Release()
{
}