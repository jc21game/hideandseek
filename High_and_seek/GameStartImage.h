#pragma once

#include "Engine/GameObject/GameObject.h"


//◆◆◆を管理するクラス

class GameStartImage : public IGameObject
{
private:
	int hPict_;    //画像番号
public:
	int count;
	//コンストラクタ
	GameStartImage(IGameObject* parent);

	//デストラクタ
	~GameStartImage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};