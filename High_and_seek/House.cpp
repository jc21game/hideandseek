#include "House.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
House::House(IGameObject * parent)
	:IGameObject(parent, "House"), hModel_(-1)
{
}

//デストラクタ
House::~House()
{
}

//初期化
void House::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/material/Floor.fbx");
	assert(hModel_ >= 0);
}

//更新
void House::Update()
{
}

//描画
void House::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void House::Release()
{
}