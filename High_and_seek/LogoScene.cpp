#include "SplashScene.h"
#include "Engine/global.h"
#include "LogoScene.h"
#include "Engine/ResouceManager/Image.h"

LogoScene::LogoScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), hPict_(-1)
{
}

void LogoScene::Initialize()
{
	//画像ロード
	hPict_ = Image::Load("data/Logo/Touhoku.png");
	assert(hPict_ >= 0);
}

void LogoScene::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

void LogoScene::Draw()
{
	//画像出すとき
	Image::SetMatrix(hPict_, localMatrix_);
	Image::Draw(hPict_);
}

void LogoScene::Release()
{
}
