#pragma once
#include "Engine/GameObject/GameObject.h"

//Captureを管理するクラス
class Capture : public IGameObject
{
	int hModel_;    //モデル番号
	D3DXVECTOR3 move_;	//移動
	float DownY_;			//Ｙ方向の加速度

	const float SPEED;	//速度

public:
	//コンストラクタ
	Capture(IGameObject* parent);

	//デストラクタ
	~Capture();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//移動ベクトルのセッター
	void SetMove(D3DXVECTOR3 move)
	{
		D3DXVec3Normalize(&move, &move);
		move_ = move * 0.9;

	}

	//発射
	//引数；position	発射位置
	//引数：direction	発射方向
	void Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction);
};