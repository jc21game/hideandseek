#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	Text* pText_;    //テキスト
	int hPict_;    //画像番号

public:
	//コンストラクタ
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};