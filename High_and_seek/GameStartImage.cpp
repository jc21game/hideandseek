#include "GameStartImage.h"
#include "Engine/ResouceManager/Image.h"
#include "MenuScene.h"

//コンストラクタ
GameStartImage::GameStartImage(IGameObject * parent)
	:IGameObject(parent, "GameStartImage"), hPict_(-1)
{
}

//デストラクタ
GameStartImage::~GameStartImage()
{
}

//初期化
void GameStartImage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/GameStartImage.png");
	assert(hPict_ >= 0);
}

//更新
void GameStartImage::Update()
{
	//↓が押されていたら
	if (Input::IsKeyDown(DIK_DOWN))
	{
		count++;
	}
	//↑が押されていたら
	if (Input::IsKeyDown(DIK_UP))
	{
		count++;
	}

	//画像削除
	if (count == 2)
	{
		count = 0;
		KillMe();
	}
}

//描画
void GameStartImage::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GameStartImage::Release()
{
}