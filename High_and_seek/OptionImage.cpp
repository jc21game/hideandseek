#include "OptionImage.h"
#include "Engine/ResouceManager/Image.h"
#include "MenuScene.h"

//コンストラクタ
OptionImage::OptionImage(IGameObject * parent)
	:IGameObject(parent, "OptionImage"), hPict_(-1)
{
}

//デストラクタ
OptionImage::~OptionImage()
{
}

//初期化
void OptionImage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/OptionImage.png");
	assert(hPict_ >= 0);
}

//更新
void OptionImage::Update()
{
	//↓が押されていたら
	if (Input::IsKeyDown(DIK_DOWN))
	{
		count++;
	}
	//↑が押されていたら
	if (Input::IsKeyDown(DIK_UP))
	{
		count++;
	}

	//画像削除
	if (count == 2)
	{
		count = 0;
		KillMe();
	}
}

//描画
void OptionImage::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OptionImage::Release()
{
}