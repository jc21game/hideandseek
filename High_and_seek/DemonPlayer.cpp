#include "DemonPlayer.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
DemonPlayer::DemonPlayer(IGameObject * parent)
	:IGameObject(parent, "DemonPlayer"), hModel_(-1)
{
}

//デストラクタ
DemonPlayer::~DemonPlayer()
{
}

//初期化
void DemonPlayer::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/DemonPlayer.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(2, 0, -2), D3DXVECTOR3(1, 2, 1));
	AddCollider(collision);

}

//更新
void DemonPlayer::Update()
{
}

//描画
void DemonPlayer::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void DemonPlayer::Release()
{
}