#include "OptionScene.h"
#include "Engine/global.h"

OptionScene::OptionScene(IGameObject * parent)
	: IGameObject(parent, "OptionScene"), pText_(nullptr)
{
}

void OptionScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
}

void OptionScene::Update()
{
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

void OptionScene::Draw()
{
	pText_->Draw(100, 100, "オプションシーン");
}

void OptionScene::Release()
{
	delete pText_;
}
