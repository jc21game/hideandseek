#pragma once
#include "Engine/global.h"
#include <time.h>

class LogoScene : public IGameObject
{
	int hPict_;

	double startTime, endTime;
	double totalTime = 0.0, setTime;

public:
	LogoScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};
