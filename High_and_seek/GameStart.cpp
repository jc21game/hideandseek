#include "GameStart.h"
#include "Engine/ResouceManager/Image.h"


//コンストラクタ
GameStart::GameStart(IGameObject * parent)
	:IGameObject(parent, "GameStart"), hPict_(-1)
{
}

//デストラクタ
GameStart::~GameStart()
{
}

//初期化
void GameStart::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/GameStart.png");
	assert(hPict_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(1600.0f, 200.0f, 0);
}

//更新
void GameStart::Update()
{
}

//描画
void GameStart::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GameStart::Release()
{
}