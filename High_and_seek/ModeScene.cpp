#include "ModeScene.h"
#include "Engine/global.h"

ModeScene::ModeScene(IGameObject * parent)
	: IGameObject(parent, "ModeScene"), pText_(nullptr)
{
}

void ModeScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
}

void ModeScene::Update()
{
	bool mode = true;

	if (Input::IsKeyDown(DIK_UP))		mode = true;
	if (Input::IsKeyDown(DIK_DOWN))		mode = false;


	if (Input::IsKeyDown(DIK_SPACE))
	{
		if(mode == true)
		{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		if (mode == false)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}

	}
}

void ModeScene::Draw()
{
	pText_->Draw(100, 100, "これはMode用のシーンですSPACEで決定。十字で移動,現在どちらも同じPlayシーン");
}

void ModeScene::Release()
{
	delete pText_;
}
