#include "Chair.h"
#include "Engine/ResouceManager/Model.h"
#include "Stage.h"

//コンストラクタ
Chair::Chair(IGameObject * parent)
	:IGameObject(parent, "Chair"),hModel_(-1)
{
}

//デストラクタ
Chair::~Chair()
{
}

//初期化
void Chair::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/furniture/isu.fbx");
	assert(hModel_ >= 0);

	//SetPosition(D3DXVECTOR3(10, 1, 10));
	//コライダーの「中心位置」と「サイズ」
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(2, 0, -2), D3DXVECTOR3(1, 2, 1));
	AddCollider(collision);

}

//更新
void Chair::Update()
{
}

//描画
void Chair::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Chair::Release()
{
}