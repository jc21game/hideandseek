#include "CharacterSelect.h"
#include "Engine/ResouceManager/Image.h"


//コンストラクタ
CharacterSelect::CharacterSelect(IGameObject * parent)
	:IGameObject(parent, "CharacterSelect"), hPict_(-1)
{
}

//デストラクタ
CharacterSelect::~CharacterSelect()
{
}

//初期化
void CharacterSelect::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/CharacterSelect.png");
	assert(hPict_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(1600.0f, 400.0f, 0);
}

//更新
void CharacterSelect::Update()
{
}

//描画
void CharacterSelect::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void CharacterSelect::Release()
{
}