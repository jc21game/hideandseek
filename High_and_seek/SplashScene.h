#pragma once

#include "Engine/global.h"

class SplashScene : public IGameObject
{
public:
	SplashScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};
