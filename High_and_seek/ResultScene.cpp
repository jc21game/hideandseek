#include "ResultScene.h"
#include "Engine/global.h"
#include "Engine/ResouceManager/Image.h"

ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), pText_(nullptr), hPict_(-1)
{
}

void ResultScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);

	//画像データのロード
	hPict_ = Image::Load("data/playstation.jfif");
	assert(hPict_ >= 0);
}

void ResultScene::Update()
{
	if (Input::IsKeyDown(DIK_R))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
	pText_->Draw(100, 100, "これはリザルト用のシーンです。Rでリトライ,SPACEでタイトル");
}

void ResultScene::Release()
{
	delete pText_;
}
