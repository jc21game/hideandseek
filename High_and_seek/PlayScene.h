#pragma once
#include <time.h>
#include <string>
#include "Engine/global.h"
#include "Engine/DirectX/Text.h"

using namespace std;


class PlayScene : public IGameObject
{

    enum STATE
    {
	     STATE_HIDETURN,//隠すターン
	     STATE_FINDTURN //探すターン
    }state_;

	//隠すプレイヤーの行動
	enum STATE_HIDEPLAYER
	{
		STATE_HIDEPLAYER_MOVE,
		STATE_HIDEPLAYER_FINISH,


	}statehide_;

	//探すプレイヤーの行動
	enum STATE_FINDPLAYER_MOVE
	{
		STATE_FINDPLAYER_MOVE


	}statefind_;

	struct timer
	{
		int min_;  //100の位
		int tens_; //10の位
		int sec_;  //1の位
	}timer_;

	Text* pText_;      //テキスト
	Text* pMathText_;  //数字テキスト

	//表示する文字列の配列
	string number_[10] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

	//表示する文字列の格納
	string str;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測


	bool initFlag_;

public:
	PlayScene(IGameObject* parent);


	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//時間をカウントする関数
	void CountTimer();
	//描画
	void Draw() override;
	//開放
	void Release() override;
	//隠す側の時間初期化
	void SetHideTime();
	//探す側の時間初期化
	void SetFindTime();
	//状態の終了処理
	void ChangeState(STATE state);
	//時間をセットする
	//引数 min：百の位 tens：十の位 sac：一の位
	void SetTimer(int min, int tens, int sec);

	timer GetTimer()
	{
		return timer_;
	}



};