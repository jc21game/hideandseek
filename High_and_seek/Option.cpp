#include "Option.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Option::Option(IGameObject * parent)
	:IGameObject(parent, "Option"), hPict_(-1)
{
}

//デストラクタ
Option::~Option()
{
}

//初期化
void Option::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/Option.png");
	assert(hPict_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(1600.0f, 600.0f, 0);
}

//更新
void Option::Update()
{
}

//描画
void Option::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Option::Release()
{
}