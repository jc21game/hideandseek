#include "Capture.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Capture::Capture(IGameObject * parent)
	:IGameObject(parent, "Capture"), hModel_(-1)
	, move_(D3DXVECTOR3(0, 0, 0)), DownY_(0.0f), SPEED(0.7f)
{
}

//デストラクタ
Capture::~Capture()
{
}

//初期化
void Capture::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/Player.fbx");
	assert(hModel_ >= 0);

	//当たり判定をつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.8f);
	AddCollider(collision);	//コライダー
}

//更新
void Capture::Update()
{
	//弾の速度
/*	position_ += _move;

	_move.y += _DownY;
	_DownY -= 0.0009f;
	*/
	//授業用	
	position_ += move_;
	move_.y -= 0.01f;
	//ジャンプするときyを５にして、yを-1ずつ(重力)減らせばよい


	//ある程度低い位置まで落ちたら消す
	if (position_.y < -15)
	{
		KillMe();
	}


}

//描画
void Capture::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Capture::Release()
{
}

//発射
void Capture::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&move_, &direction);
	move_ *= SPEED;
}