#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class DemonPlayer : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	DemonPlayer(IGameObject* parent);

	//デストラクタ
	~DemonPlayer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//int GetModelHandle() { return hModel_; }
};