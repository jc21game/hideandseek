#include "Audio.h"

//Sound*	Audio::pSound_ = nullptr;

//3D画像を管理する
namespace Audio
{
	//サウンドクラスのオブジェクト
	Sound* pSound_;

	//ロード済みの画像データ一覧
	std::vector<AudioData*>	datas_;


	//初期化
	void Initialize(HWND hWnd)
	{
		pSound_ = new Sound;
		pSound_->Initialize(hWnd);
		AllRelease();
	}


	//サウンドファイルをロード
	int Load(std::string fileName)
	{
		AudioData* pData = new AudioData;

		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && datas_[i]->fileName == fileName)
			{
				pData->psoundBuffer = datas_[i]->psoundBuffer;
				isExist = true;
				break;
			}
		}

		//新たにファイルを開く
		if (isExist == false)
		{
			pData->psoundBuffer = new LPDIRECTSOUNDBUFFER;
			if (FAILED(pSound_->Load(pData->psoundBuffer, fileName)))
			{
				//開けなかった
				SAFE_DELETE(pData);
				return -1;
			}

			//無事開けた
			pData->fileName = fileName;
		}


		//使ってない番号が無いか探す
		for (int i = 0; i < datas_.size(); i++)
		{
			if (datas_[i] == nullptr)
			{
				datas_[i] = pData;
				return i;
			}
		}

		//新たに追加
		datas_.push_back(pData);
		return datas_.size() - 1;
	}



	//再生
	void Play(int handle)
	{
		if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
		{
			return;
		}
		pSound_->Play(datas_[handle]->psoundBuffer);

	}


	//停止
	void Stop(int handle)
	{
		if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
		{
			return;
		}
		pSound_->Stop(datas_[handle]->psoundBuffer);
	}



	//任意のサウンドを開放
	void Release(int handle)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		//同じサウンドを他でも使っていないか
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && i != handle && datas_[i]->psoundBuffer == datas_[handle]->psoundBuffer)
			{
				isExist = true;
				break;
			}
		}

		//使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE(datas_[handle]->psoundBuffer);
		}


		SAFE_DELETE(datas_[handle]);
	}



	//全てのサウンドを解放
	void AllRelease()
	{
		for (int i = 0; i < datas_.size(); i++)
		{
			Release(i);
		}
		datas_.clear();
	}


	//ゲーム終了時に行う処理
	void ReleaseDirectSound()
	{
		SAFE_DELETE(pSound_);
	}
}

