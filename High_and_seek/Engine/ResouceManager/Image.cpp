#include "Image.h"

//3D画像を管理する
namespace Image
{
	//ロード済みの画像データ一覧
	std::vector<ImageData*>	datas_;

	//初期化
	void Initialize()
	{
		AllRelease();
	}

	//画像をロード
	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;

		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && datas_[i]->fileName == fileName)
			{
				pData->pSprite = datas_[i]->pSprite;
				isExist = true;
				break;
			}
		}

		//新たにファイルを開く
		if (isExist == false)
		{
			pData->pSprite = new Sprite;
			if (FAILED(pData->pSprite->Load(fileName)))
			{
				//開けなかった
				SAFE_DELETE(pData->pSprite);
				SAFE_DELETE(pData);
				return -1;
			}

			//無事開けた
			pData->fileName = fileName;
		}


		//使ってない番号が無いか探す
		for (int i = 0; i < datas_.size(); i++)
		{
			if (datas_[i] == nullptr)
			{
				datas_[i] = pData;
				return i;
			}
		}

		//新たに追加
		datas_.push_back(pData);

		//画像番号割り振り
		int handle = datas_.size() - 1;

		//切り抜き範囲をリセット
		ResetRect(handle);

		return handle;
	}



	//描画
	void Draw(int handle)
	{
		if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
		{
			return;
		}
		datas_[handle]->pSprite->Draw(datas_[handle]->matrix, &datas_[handle]->rect);
	}



	//任意の画像を開放
	void Release(int handle)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && i != handle && datas_[i]->pSprite == datas_[handle]->pSprite)
			{
				isExist = true;
				break;
			}
		}

		//使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE(datas_[handle]->pSprite);
		}

		SAFE_DELETE(datas_[handle]);
	}



	//全ての画像を解放
	void AllRelease()
	{
		for (int i = 0; i < datas_.size(); i++)
		{
			Release(i);
		}
		datas_.clear();
	}


	//切り抜き範囲の設定
	void SetRect(int handle, int x, int y, int width, int height)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		datas_[handle]->rect.left = x;
		datas_[handle]->rect.top = y;
		datas_[handle]->rect.right = x + width;
		datas_[handle]->rect.bottom = y + height;
	}


	//切り抜き範囲をリセット（画像全体を表示する）
	void ResetRect(int handle)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		D3DXVECTOR2 size = datas_[handle]->pSprite->GetTextureSize();

		datas_[handle]->rect.left = 0;
		datas_[handle]->rect.top = 0;
		datas_[handle]->rect.right = size.x;
		datas_[handle]->rect.bottom = size.y;

	}


	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX matrix)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		datas_[handle]->matrix = matrix;
	}


	//ワールド行列の取得
	D3DXMATRIX GetMatrix(int handle)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			D3DXMATRIX mat;
			D3DXMatrixIdentity(&mat);
			return mat;
		}
		return datas_[handle]->matrix;
	}
}

