#include <Windows.h>
#include <stdlib.h>
#include <assert.h>
#include "global.h"
#include "ResouceManager/Model.h"
#include "ResouceManager/Image.h"
#include "ResouceManager/Audio.h"
#include "ResouceManager/Movie.h"

//定数宣言
const char* WIN_CLASS_NAME = "SampleGame";	//ウィンドウクラス名


//プロトタイプ宣言
HWND InitApp(HINSTANCE hInstance, int screenWidth, int screenHeight, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


// エントリーポイント
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(117311);
#endif

	srand((unsigned)time(NULL));

	//ウィンドウのサイズ
	int ScreenWidth = GetPrivateProfileInt("SCREEN", "Width", 800, ".\\Data\\setup.ini");
	int ScreenHeight = GetPrivateProfileInt("SCREEN", "Height", 600, ".\\Data\\setup.ini");


	//ウィンドウの作成
	HWND hWnd = InitApp(hInstance, ScreenWidth, ScreenHeight, nCmdShow);

	//Direct3D準備
	Direct3D::Initialize(hWnd, ScreenWidth, ScreenHeight);

	//DirectInput（キーボード）準備
	Input::Initialize(hWnd);

	//3Dモデルの準備
	Model::Initialize();

	//2D画像の準備
	Image::Initialize();

	//オーディオの準備
	Audio::Initialize(hWnd);

	//ルートオブジェクトと、その子供のシーンマネージャーを作成
	GameObjectManager::Initialize();

	//ムービーの準備
	Movie::Initialize(hWnd);

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり（こっちが優先）
		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし（ここでゲームの処理）
		else
		{
			//キーボード情報の更新
			Input::Update();

			//ルートの更新（子供のUpdateも次々呼ばれる）
			GameObjectManager::Update();

			//このフレームの描画開始
			Direct3D::BeginDraw();

			//ルートの描画（子供のDrawも次々呼ばれる）
			GameObjectManager::Draw();

			//描画終了
			Direct3D::EndDraw();
		}
	}


	GameObjectManager::Release();
	Audio::AllRelease();
	Audio::ReleaseDirectSound();
	Model::AllRelease();
	Image::AllRelease();
	Movie::Release();
	Input::Release();
	Direct3D::Release();
	return 0;
}


//ウィンドウの作成
HWND InitApp(HINSTANCE hInstance, int screenWidth, int screenHeight, int nCmdShow)
{
	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);				//この構造体のサイズ
	wc.hInstance = hInstance;					//インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;			//ウィンドウクラス名
	wc.lpfnWndProc = WndProc;					//ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;			//スタイル（デフォルト）
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);	//アイコン
	wc.hIconSm = LoadIcon(nullptr, IDI_WINLOGO);	//小さいアイコン
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);	//マウスカーソル
	wc.lpszMenuName = nullptr;						//メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	RegisterClassEx(&wc);

	//ウィンドウサイズの計算
	RECT winRect = { 0, 0, screenWidth, screenHeight };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);

	//タイトルバーに表示する内容
	char caption[64];
	GetPrivateProfileString("SCREEN", "Caption", "***", caption, 64, ".\\Data\\setup.ini");

	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,					//ウィンドウクラス名
		caption,						//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,			//スタイル（普通のウィンドウ）
		CW_USEDEFAULT,					//表示位置左（おまかせ）
		CW_USEDEFAULT,					//表示位置上（おまかせ）
		winRect.right - winRect.left,	//ウィンドウ幅
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		nullptr,							//親ウインドウ（なし）
		nullptr,							//メニュー（なし）
		hInstance,						//インスタンス
		nullptr							//パラメータ（なし）
	);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	return hWnd;
}


//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);	//プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}