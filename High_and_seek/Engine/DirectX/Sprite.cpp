#include "Sprite.h"

//コンストラクタ
Sprite::Sprite():
	pSprite_(nullptr),
	pTexture_(nullptr)
{

}

//デストラクタ
Sprite::~Sprite()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}


//画像ファイルの読み込み
HRESULT Sprite::Load(std::string fileName)
{
	//スプライトオブジェクト作成
	D3DXCreateSprite(Direct3D::pDevice_, &pSprite_);

	//テクスチャオブジェクトの作成
	HRESULT hr;
	hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice_, fileName.c_str(), 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pTexture_);

	return hr;
}


//描画
void Sprite::Draw(D3DXMATRIX & matrix, RECT* rect)
{
	//変換行列をセット
	pSprite_->SetTransform(&matrix);

	//描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, rect, nullptr, nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	pSprite_->End();
}

//テクスチャのサイズを取得
D3DXVECTOR2 Sprite::GetTextureSize()
{
	D3DXVECTOR2 size;
	D3DSURFACE_DESC d3dds;
	pTexture_->GetLevelDesc(0, &d3dds);
	size.x = d3dds.Width;
	size.y = d3dds.Height;

	return size;
}
