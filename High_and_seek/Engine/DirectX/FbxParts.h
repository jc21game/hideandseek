#include <d3dx9.h>
#include <fbxsdk.h>
#include <vector>
#include "../global.h"


class Fbx;
struct RayCastData;

//FBXの中の１つのパーツを扱うクラス
class FbxParts
{
public:
	//頂点データ構造体
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	// ボーン構造体（関節情報）
	struct  Bone
	{
		D3DXMATRIX  bindPose;      // 初期ポーズ時のボーン変換行列
		D3DXMATRIX  newPose;       // アニメーションで変化したときのボーン変換行列
		D3DXMATRIX  diffPose;      // mBindPose に対する mNowPose の変化量
	};

	// ウェイト構造体（ボーンと頂点の関連付け）
	struct Weight
	{
		D3DXVECTOR3 posOrigin;		// 元々の頂点座標
		D3DXVECTOR3 normalOrigin;	// 元々の法線ベクトル
		int*		pBoneIndex;		// 関連するボーンのID
		float*		pBoneWeight;	// ボーンの重み
	};

private:
	//各データの個数
	int vertexCount_;		//頂点数
	int polygonCount_;		//ポリゴ数
	int indexCount_;		//インデックス数
	int materialCount_;		//マテリアルの個数
	int* pPolygonCountOfMaterial_;	//マテリアルごとのポリゴン数


	//Fbxから読み取る各種情報
	Vertex*						pVertexList_;	//頂点情報
	LPDIRECT3DVERTEXBUFFER9		vertexBuffer_;	//頂点バッファ
	LPDIRECT3DINDEXBUFFER9*		pIndexBuffer_;	//インデックスバッファ
	D3DMATERIAL9*				pMaterial_;		//マテリアル
	LPDIRECT3DTEXTURE9*			pTexture_;		//テクスチャ


	// ボーン制御情報
	FbxSkin*		pSkinInfo_;    // スキンメッシュ情報（スキンメッシュアニメーションのデータ本体）
	FbxCluster**	ppCluster_;    // クラスタ情報（関節ごとに関連付けられた頂点情報）
	int				numBone_;      // FBXに含まれている関節の数
	Bone*			pBoneArray_;    // 各関節の情報
	Weight*			pWeightArray_;  // ウェイト情報（頂点の対する各関節の影響度合い）

	//ノードのポインタ
	FbxNode* pNode_;

	//アニメーション用行列
	D3DXMATRIX localMatrix_;


	//子供のパーツ
	std::vector<FbxParts*>	childParts_;

	//パーツを描画（実際に描画しているのはここ）
	//引数：matrix　ワールド行列
	void Draw(D3DXMATRIX &matrix);

public:
	FbxParts();
	~FbxParts();

	//マテリアル（色や質感、テクスチャ）の情報をロード
	//引数：pNode	1パーツの情報
	//引数：pFbx	呼び出し元のFBXオブジェクトのアドレス
	void InitMaterial(FbxNode *pNode, Fbx* pFbx);

	//頂点バッファの作成
	//引数：pMesh	メッシュ情報
	void InitVertexBuffer(FbxMesh* pMesh);

	//インデックスバッファの作成
	//引数：pMesh	メッシュ情報
	void InitIndexBuffer(FbxMesh* pMesh);

	//アニメーションデータの準備
	//引数：pMesh	メッシュ情報
	void InitAnimation(FbxMesh* pMesh);

	//ボーン有りのモデルを描画
	//引数：matrix	ワールド行列
	//引数：time	フレーム情報（１アニメーション内の今どこか）
	void DrawSkinAnime(D3DXMATRIX &matrix, FbxTime time);

	//ボーン無しのモデルを描画
	//引数：matrix	ワールド行列
	//引数：time	フレーム情報（１アニメーション内の今どこか）
	//引数：scene	Fbxファイルから読み込んだシーン情報
	void DrawMeshAnime(D3DXMATRIX &matrix, FbxTime time, FbxScene* scene);

	//任意のボーンの位置を取得
	//引数：boneName	取得したいボーンの位置
	//引数：position	ワールド座標での位置【out】
	//戻値：見つかればtrue
	bool GetBonePosition(std::string boneName, D3DXVECTOR3* position);

	//レイキャスト（レイを飛ばして当たり判定）
	//引数：data	必要なものをまとめたデータ
	void RayCast(RayCastData *data);

	//スキンメッシュ情報を取得
	//戻値：スキンメッシュ情報
	FbxSkin* GetSkinInfo() { return pSkinInfo_; }
};