#include<assert.h>
#include "Direct3D.h"



namespace Direct3D
{
	LPDIRECT3D9			pD3d_ = nullptr;	//Direct3Dオブジェクト
	LPDIRECT3DDEVICE9	pDevice_ = nullptr;	//Direct3Dデバイスオブジェクト
	float				aspect_ = 1.0f;		//スクリーンのアスペクト比	
	bool		isDrawCollision_ = false;	//コリジョンを表示するか
	bool		isLighting_= false;			//ライティングするか

	void Initialize(HWND hWnd, int screenWidth, int screenHeight)
	{
		HRESULT hr;

		//Direct3Dオブジェクトの作成
		pD3d_ = Direct3DCreate9(D3D_SDK_VERSION);
		assert(pD3d_ >= 0);


		//DIRECT3Dデバイスオブジェクトの作成
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferCount = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.Windowed = GetPrivateProfileInt("SCREEN", "FullScreen", 0, ".\\Data\\setup.ini") != 1;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.BackBufferWidth = screenWidth;
		d3dpp.BackBufferHeight = screenHeight;
		d3dpp.hDeviceWindow = hWnd;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		// アンチエイリアシング
		if (GetPrivateProfileInt("RENDER", "AntiAliasing", 0, ".\\Data\\setup.ini") != 0)
		{
			DWORD QualityBackBuffer = 0;
			pD3d_->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dpp.BackBufferFormat, 
				d3dpp.Windowed, D3DMULTISAMPLE_4_SAMPLES, &QualityBackBuffer);
			d3dpp.MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
			d3dpp.MultiSampleQuality = QualityBackBuffer - 1;
		}

		hr = pD3d_->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice_);
		assert(pDevice_ > 0);


		//アスペクト比
		aspect_ = (float)screenWidth / (float)screenHeight;

		//アルファブレンド
		pDevice_->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		pDevice_->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pDevice_->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

		//ライティング
		isLighting_ = GetPrivateProfileInt("RENDER", "Lighting", 0, ".\\Data\\setup.ini") != 0;
		if (isLighting_)
		{
			pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);
			D3DLIGHT9 lightState;
			ZeroMemory(&lightState, sizeof(lightState));

			lightState.Type = D3DLIGHT_DIRECTIONAL;

			lightState.Direction = D3DXVECTOR3(1, -1, 1);

			lightState.Diffuse.r = 1.0f;
			lightState.Diffuse.g = 1.0f;
			lightState.Diffuse.b = 1.0f;

			lightState.Ambient.r = 1.0f;
			lightState.Ambient.g = 1.0f;
			lightState.Ambient.b = 1.0f;

			pDevice_->SetLight(0, &lightState);

			pDevice_->LightEnable(0, TRUE);
		}
		else
		{
			pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);
		}

		//カメラ
		D3DXMATRIX view, proj;
		D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
		D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)screenWidth / screenHeight, 0.5f, 1000.0f);
		pDevice_->SetTransform(D3DTS_VIEW, &view);
		pDevice_->SetTransform(D3DTS_PROJECTION, &proj);

		//コリジョン表示するか
		isDrawCollision_ = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\Data\\setup.ini") != 0;


	}

	//描画開始
	void BeginDraw()
	{
		//画面をクリア
		pDevice_->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

		//描画開始
		pDevice_->BeginScene();
	}

	//描画終了
	void EndDraw()
	{
		//描画終了
		pDevice_->EndScene();

		//スワップ
		pDevice_->Present(nullptr, nullptr, nullptr, nullptr);
	}

	//開放処理
	void Release()
	{
		pDevice_->Release();
		pD3d_->Release();
	}

}