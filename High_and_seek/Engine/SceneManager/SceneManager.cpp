#include "sceneManager.h"
#include "../global.h"
#include "../ResouceManager/Model.h"
#include "../ResouceManager/Image.h"

#include "../../SplashScene.h"
#include "../../PlayScene.h"
#include "../../TitleScene.h"
#include "../../LogoScene.h"
#include "../../MenuScene.h"
#include "../../CharacterSelectScene.h"
#include "../../OptionScene.h"
#include "../../OperationMethodScene.h"

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	: IGameObject(parent, "SceneManager")
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_SPLASH;
	nextSceneID_ = SCENE_ID_SPLASH;
	CreateGameObject<SplashScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (nextSceneID_) 
		{
		case SCENE_ID_SPLASH: CreateGameObject<SplashScene>(this); break;
		case SCENE_ID_TITLE: CreateGameObject<TitleScene>(this); break;
		case SCENE_ID_PLAY: CreateGameObject<PlayScene>(this); break;
		case SCENE_ID_LOGO: CreateGameObject<LogoScene>(this); break;
		case SCENE_ID_MENU: CreateGameObject<MenuScene>(this); break;
		case SCENE_ID_CHAR: CreateGameObject<CharacterSelectScene>(this); break;
		case SCENE_ID_OPTION: CreateGameObject<OptionScene>(this); break;
		case SCENE_ID_OPERATIONMETHOD: CreateGameObject<OperationMethodScene>(this); break;
		}

		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}