#pragma once

#include <d3dx9.h>


class IGameObject;

//あたり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE		//球体
};


//あたり判定を管理するクラス
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;

protected:
	IGameObject*	pGameObject_;	//この判定をつけたゲームオブジェクト
	ColliderType	type_;			//種類
	D3DXVECTOR3		center_;		//中心位置（ゲームオブジェクトの原点から見た位置）
	LPD3DXMESH		pMesh_;			//テスト表示用の枠

public:
	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target) = 0;

	//箱型同士の衝突判定
	//引数：boxA	１つ目の箱型判定
	//引数：boxB	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱型と球体の衝突判定
	//引数：box	箱型判定
	//引数：sphere	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere);

	//球体同士の衝突判定
	//引数：circleA	１つ目の球体判定
	//引数：circleB	２つ目の球体判定
	//戻値：接触していればtrue
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);

	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw(D3DXVECTOR3 position);

	//セッター
	void SetGameObject(IGameObject* gameObject) { pGameObject_ = gameObject; }

};

