//#include <mmsystem.h >
#include "gameObjectManager.h"
#include "gameObject.h"
#include <list>
#include "../global.h"

#pragma comment(lib, "winmm.lib")

namespace
{
	//ルートオブジェクトのクラス
	class RootJob : public IGameObject 
	{
	public:
		RootJob() : IGameObject(nullptr, "RootJob") {}
		~RootJob() {}
		void Initialize() {}
		void Update() {}
		void Draw() {}
		void Release() {}
	};


	//ルートオブジェクト
	RootJob* rootJob = nullptr;

	//更新処理
	//引数：obj	対象のオブジェクト
	void UpdateSub(IGameObject* obj)
	{
		//更新フラグがfalseなら終了
		if (!obj->IsEntered())
			return;

		//対象オブジェクトの更新処理
		obj->Update();
		obj->Transform();

		//その子オブジェクトの更新処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end();) 
		{
			//まだ初期化してなかったら初期化する
			if (!(*it)->IsInitialized()) 
			{
				(*it)->SetInitialized();	//初期化フラグを立てる
				(*it)->Initialize();		//初期化
			}

			//更新処理
			UpdateSub(*it);

			//削除フラグが立ってたら削除
			if ((*it)->IsDead()) 
			{
				(*it)->Release();
				SAFE_DELETE(*it);
				it = list->erase(it);
			}
			else
			{
				//当たり判定
				(*it)->Collision(rootJob);


				it++;
			}
		}
	}


	//描画処理
	//引数：obj	対象のオブジェクト
	void DrawSub(IGameObject* obj)
	{
		//描画フラグがfalseなら終了
		if (!obj->IsVisibled())
			return;

		//対象オブジェクトの描画
		obj->Draw();


//リリース時は削除
#ifdef _DEBUG
		//コリジョンの描画
		if (Direct3D::isDrawCollision_)
		{
			obj->CollisionDraw();
		}
#endif


		//その子オブジェクトの描画処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++) 
		{
			DrawSub(*it);
		}
	}


	//開放処理
	//引数：obj	対象のオブジェクト
	void ReleaseSub(IGameObject* obj)
	{
		//まずは子オブジェクトを開放
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++) {
			ReleaseSub(*it);
		}

		//対象オブジェクトを開放
		obj->Release();
		SAFE_DELETE(obj);
	}
}


//ゲームオブジェクトを管理する関数達
namespace GameObjectManager
{
	//FPSを表示するか
	bool isDrawFps_ = false;


	//初期化
	void Initialize()
	{
		//ルートオブジェクト作成
		rootJob = new RootJob;

		//シーンマネージャー作成
		CreateGameObject<SceneManager>(rootJob);

		//FPSを表示するか
		isDrawFps_ = GetPrivateProfileInt("DEBUG", "ViewFps", 0, ".\\Data\\setup.ini") != 0;

	}

	//更新
	void Update()
	{

//リリース時は削除
#ifdef _DEBUG
		static int FPS = 0;
		static int lastFpsResetTime = timeGetTime();
		static float lastUpdateTime = timeGetTime();
		float nowTime = timeGetTime();

		if (nowTime - lastFpsResetTime > 1000)
		{
			if (isDrawFps_)
			{
				char string[16];
				wsprintf(string, "FPS:%d", FPS);
				SetWindowText(GetActiveWindow(), string);
			}

			FPS = 0;
			lastFpsResetTime = nowTime;
		}
		FPS++;
#endif

		//ルートの更新
		UpdateSub(rootJob);

	}


	//描画
	void Draw()
	{
		//ルートの描画
		DrawSub(rootJob);
	}


	//開放
	void Release()
	{
		//ルートの開放
		ReleaseSub(rootJob);
	}


	//ルートの子供としてオブジェクトを追加
	//引数：追加するオブジェクト（実態はSceneManager）
	void PushBackChild(IGameObject * obj)
	{
		rootJob->GetChildList()->push_back(obj);
	}


	//名前でオブジェクトを検索
	//引数：探したい名前
	//引数：見つけたオブジェクトのアドレス（見つからなければnullptr）
	IGameObject * FindChildObject(const std::string & name)
	{
		return rootJob->FindChildObject(name);
	}

}
