#pragma once
#include "../gameObject/gameObject.h"

//カメラクラス
class Camera : public IGameObject
{
private:
	D3DXVECTOR3 position_;	//カメラの位置
	D3DXVECTOR3 target_;	//カメラの焦点

public:
	Camera(IGameObject* parent);
	~Camera();

	//初期化（プロジェクション行列作成）
	void Initialize() override;

	//更新（ビュー行列作成）
	void Update() override;

	//描画（必要ないけどオーバーライドしないといけない）
	void Draw() override {};

	//開放（必要ないけどオーバーライドしないといけない）
	void Release() override {};

	//焦点を設定
	void SetTarget(D3DXVECTOR3 target) { target_ = target; }

	//位置を設定
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
};