#include "BoxCollider.h"
#include "SphereCollider.h"
#include "gameObject.h"
#include "../DirectX/Direct3D.h"

//コンストラクタ
Collider::Collider():
	pGameObject_(nullptr),pMesh_(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

//箱型同士の衝突判定
//引数：boxA	１つ目の箱型判定
//引数：boxB	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB)
{
	D3DXVECTOR3 boxPosA = boxA->pGameObject_->GetPosition() + boxA->center_;
	D3DXVECTOR3 boxPosB = boxB->pGameObject_->GetPosition() + boxB->center_;

	if ((boxPosA.x + boxA->size_.x / 2) > (boxPosB.x - boxB->size_.x / 2) &&
		(boxPosA.x - boxA->size_.x / 2) < (boxPosB.x + boxB->size_.x / 2) &&
		(boxPosA.y + boxA->size_.y / 2) > (boxPosB.y - boxB->size_.y / 2) &&
		(boxPosA.y - boxA->size_.y / 2) < (boxPosB.y + boxB->size_.y / 2) &&
		(boxPosA.z + boxA->size_.z / 2) > (boxPosB.z - boxB->size_.z / 2) &&
		(boxPosA.z - boxA->size_.z / 2) < (boxPosB.z + boxB->size_.z / 2))
	{
		return true;
	}
	return false;
}

//箱型と球体の衝突判定
//引数：box	箱型判定
//引数：sphere	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere)
{
	D3DXVECTOR3 circlePos = sphere->pGameObject_->GetPosition() + sphere->center_;
	D3DXVECTOR3 boxPos = box->pGameObject_->GetPosition() + box->center_;


	if (circlePos.x > boxPos.x - box->size_.x - sphere->radius_ &&
		circlePos.x < boxPos.x + box->size_.x + sphere->radius_ &&
		circlePos.y > boxPos.y - box->size_.y - sphere->radius_ &&
		circlePos.y < boxPos.y + box->size_.y + sphere->radius_ &&
		circlePos.z > boxPos.z - box->size_.z - sphere->radius_ &&
		circlePos.z < boxPos.z + box->size_.z + sphere->radius_ )
	{
		return true;
	}

	return false;
}

//球体同士の衝突判定
//引数：circleA	１つ目の球体判定
//引数：circleB	２つ目の球体判定
//戻値：接触していればtrue
bool Collider::IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB)
{
	D3DXVECTOR3 v = (circleA->center_ + circleA->pGameObject_->GetPosition()) 
		- (circleB->center_ + circleB->pGameObject_->GetPosition());

	if (D3DXVec3Length(&v) <= (circleA->radius_ + circleB->radius_))
	{
		return true;
	}

	return false;
}

//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}
