#include "camera.h"
#include "../DirectX/Direct3D.h"


//コンストラクタ
Camera::Camera(IGameObject* parent)
	: IGameObject(parent, "Camera"), target_(D3DXVECTOR3(0, 0, 0))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化（プロジェクション行列作成）
void Camera::Initialize()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), Direct3D::aspect_, 1.0f, 1000.0f);
	Direct3D::pDevice_->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新（ビュー行列作成）
void Camera::Update()
{
	D3DXVECTOR3 worldPos;
	D3DXVec3TransformCoord(&worldPos, &position_, &worldMatrix_);

	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);


	//ビュー行列
	D3DXMATRIX view;
	D3DXMatrixLookAtLH(&view, &worldPos, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &view);
}

