#pragma once

#include <list>
#include <string>
#include <d3dx9.h>
#include <assert.h>
#include "gameObjectManager.h"
#include "SphereCollider.h"
#include "BoxCollider.h"




//全てのゲームオブジェクト（シーンも含めて）が継承するインターフェース
// ゲームオブジェクトは、親子構造になっていて、
// マトリクスの影響を受けることになる
class IGameObject
{
protected:
	D3DXMATRIX localMatrix_;	//このオブジェクトの行列(親子で描画するため)
	D3DXMATRIX worldMatrix_;	//親の影響を受けた最終的な行列
	std::string objectName_;	//オブジェクトの名前

	D3DXVECTOR3 position_;		//位置
	D3DXVECTOR3 rotate_;		//向き
	D3DXVECTOR3 scale_;			//拡大率

	std::list<Collider*>	colliderList_;	//衝突判定リスト

public:
	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	//デストラクタ
	~IGameObject();

	//各オブジェクトで必ず作る関数
	virtual void Initialize(void) = 0;
	virtual void Update(void) = 0;
	virtual void Draw() = 0;
	virtual void Release(void) = 0;

	//ワールド行列の作成
	virtual void Transform();


	//ローカル行列の取得（このオブジェクトの行列）
	//戻値：ローカル行列
	virtual const D3DXMATRIX& GetLocalMatrix();

	//ワールド行列の取得（親の影響を受けた最終的な行列）
	//戻値：ワールド行列
	virtual const D3DXMATRIX& GetWorldMatrix();



	//各フラグの制御
	bool IsDead();	// 削除するかどうか
	void KillMe();	// 自分を削除する
	void Enter();	// Updateを許可
	void Leave();	// Updateを拒否
	void Visible();		// Drawを許可
	void Invisible();	// Drawを拒否
	bool IsInitialized();	// 初期化済みかどうか
	void SetInitialized();	// 初期化済みにする
	bool IsEntered();	// Update実行していいか
	bool IsVisibled();	// Draw実行していいか


	//子オブジェクトリストを取得
	//戻値：子オブジェクトリスト
	std::list<IGameObject*>* GetChildList();

	//親オブジェクトを取得
	//戻値：親オブジェクトのアドレス
	IGameObject* GetParent();

	//名前でオブジェクトを検索（対象は自分の子供以下）
	//引数：name	検索する名前
	//戻値：見つけたオブジェクトのアドレス（見つからなければnullptr）
	IGameObject* FindChildObject(const std::string& name);

	//名前でオブジェクトを検索（対象は全体）
	//引数：検索する名前
	//戻値：見つけたオブジェクトのアドレス
	IGameObject* FindObject(const std::string& name) { return GameObjectManager::FindChildObject(name); }

	//オブジェクトの名前を取得
	//戻値：名前
	const std::string& GetObjectName(void) const;

	//子オブジェクトを追加（リストの最後へ）
	//引数：追加するオブジェクト
	void PushBackChild(IGameObject* obj);

	//子オブジェクトを追加（リストの先頭へ）
	//引数：obj 追加するオブジェクト
	void PushFrontChild(IGameObject* obj);

	//子オブジェクトを全て削除
	void KillAllChildren();




	//コライダー（衝突判定）を追加する
	void AddCollider(Collider * collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	//引数：pTarget	衝突した相手
	virtual void OnCollision(IGameObject* pTarget) {};

	//衝突判定
	//引数：pTarget	衝突してるか調べる相手
	void Collision(IGameObject* pTarget);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();




	//各アクセス関数
	D3DXVECTOR3 GetPosition() { return position_; }
	D3DXVECTOR3 GetRotate() { return rotate_; }
	D3DXVECTOR3 GetScale() { return scale_; }
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
	void SetPosition(float x, float y, float z) { SetPosition(D3DXVECTOR3(x, y, z)); }
	void SetRotate(D3DXVECTOR3 rotate) { rotate_ = rotate; }
	void SetRotate(float x, float y, float z) { SetRotate(D3DXVECTOR3(x, y, z)); }
	void SetRotateX(float x) { SetRotate(x, rotate_.y, rotate_.z); }
	void SetRotateY(float y) { SetRotate(rotate_.x, y, rotate_.z); }
	void SetRotateZ(float z) { SetRotate(rotate_.x, rotate_.y, z); }
	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }
	void SetScale(float x, float y, float z) { SetScale(D3DXVECTOR3(x, y, z)); }


private:
	//フラグ
	struct OBJECT_STATE
	{
		unsigned initialized : 1;	//初期化済みか
		unsigned entered : 1;		//更新するか
		unsigned visible : 1;		//描画するか
		unsigned dead : 1;			//削除するか
	};
	OBJECT_STATE State_;

	//親オブジェクト
	IGameObject* Parent_;

	//子オブジェクトリスト
	std::list<IGameObject*> ChildList_;

	//オブジェクト削除（再帰）
	//引数：obj　削除するオブジェクト
	void KillObjectSub(IGameObject* obj);


};


//オブジェクトを作成するテンプレート
template <class T>
T* CreateGameObject(IGameObject* parent)
{
	T* t = new T(parent);
	if (parent != nullptr)
	{
		parent->PushBackChild(t);
	}
	else
	{
		GameObjectManager::PushBackChild(t);
	}
	return t;
}

