#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"
#include "XINPUT.h"
#include "Wall.h"

#include <time.h>
#include "furniture.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hModel_(-1), hPict_(-1), sec_(0), searchStatus(0)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/Player.fbx");
	assert(hModel_ >= 0);

	//画像データのロード
	hPict_ = Image::Load("Data/Search.png");
	assert(hPict_ >= 0);

	//カメラ
	Camera* pCamera_ = CreateGameObject<Camera>(this);
	assert((pCamera_) != nullptr);
	pCamera_->SetPosition(D3DXVECTOR3(0, 1, 0));
	pCamera_->SetTarget(D3DXVECTOR3(0, 0, 10));

}

//更新
void Player::Update()
{

	//プレイヤーの移動動作
	Player_move();
	//当たり判定の処理
	HitTest();

	Search_hide();
	Search_finding();

}

void Player::Player_move()
{
	//動く前に直前の位置を取る
	prePos = position_;

	//コントローラ
	D3DXVECTOR3 leftStick = Input::GetPadStickL();
	D3DXVECTOR3 rightStick = Input::GetPadStickR();

	move(leftStick);	//キー、コントローラの処理
	CamMove(rightStick);

	//現在の位置 - 前にいた位置でベクトルの角度を取る
	//現在と前の位置の「差」
	D3DXVECTOR3 move = position_;// - prePos;

}

void Player::move(D3DXVECTOR3 &stick)
{

	//////////////////////////////////////////////////////////////

		//回転行列を用意
	D3DXMATRIX matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(rotate_.y));

	D3DXVECTOR3 vecForward = D3DXVECTOR3(0, 0, 0.3f);			//奥移動ベクトル
	D3DXVec3TransformCoord(&vecForward, &vecForward, &matRot);

	D3DXVECTOR3 vecRight = D3DXVECTOR3(0.3f, 0, 0);				//右移動ベクトル
	D3DXVec3TransformCoord(&vecRight, &vecRight, &matRot);

	////////////////////////////////////////////////////////////////

	//押してる間ダッシュ
	if (Input::IsKey(DIK_E))
	{
		Dash = 2.0f;
	}
	else
	{
		Dash = 1.0f;
	}

	if (position_.x >= -79.0f && position_.x <= 79.0f &&
		position_.z >= -79.0f  && position_.z <= 79.0f &&
		position_.y >= -160.0f && position_.y <= 160.0f)
	{
		if (Input::IsKey(DIK_W))	position_ += vecForward * Dash;
		if (Input::IsKey(DIK_S))	position_ -= vecForward * Dash;
		if (Input::IsKey(DIK_A))	position_ -= vecRight * Dash;
		if (Input::IsKey(DIK_D))	position_ += vecRight * Dash;
		if (Input::IsKey(DIK_SPACE))position_.y += 1.0f;
		if (Input::IsKey(DIK_LSHIFT))position_.y -= 1.0f;

		//移動コントローラー
		if (stick.z > 0)	position_ += vecForward * Dash;
		if (stick.z < 0)	position_ -= vecForward * Dash;
		if (stick.x > 0)	position_ += vecRight * Dash;
		if (stick.x < 0)	position_ -= vecRight * Dash;
		if (Input::IsPadButton(XINPUT_GAMEPAD_A))position_.y += 1.0f;
		if (Input::IsPadButton(XINPUT_GAMEPAD_B))position_.y -= 1.0f;
	}
	else
	{
		if (position_.z <= -79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, -79.0f);
		if (position_.z >= 79.0f)		position_ = D3DXVECTOR3(position_.x, position_.y, 79.0f);
		if (position_.x <= -79.0f) 		position_ = D3DXVECTOR3(-79.0f, position_.y, position_.z);
		if (position_.x >= 79.0f)		position_ = D3DXVECTOR3(79.0f, position_.y, position_.z);
	}

}

//カメラ回転
void Player::CamMove(D3DXVECTOR3 &stick)
{
	//ゲームパッド
	if (stick.y > 0)		rotate_.x -= 1.0f;
	if (stick.y < 0)		rotate_.x += 1.0f;
	if (stick.x < 0)		rotate_.y -= 1.0f;
	if (stick.x > 0)		rotate_.y += 1.0f;

	//キーボード
	if (rotate_.x <= 15)
	{
		if (Input::IsKey(DIK_DOWN))rotate_.x += 1.0f;
	}

	if (rotate_.x > -15 )
	{
		if (Input::IsKey(DIK_UP))rotate_.x -= 1.0f;
	}

	if (Input::IsKey(DIK_RIGHT))	rotate_.y += 1.0f;
	if (Input::IsKey(DIK_LEFT))		rotate_.y -= 1.0f;
}

void Player::HitTest()
{
	Wall* pWall = (Wall*)FindObject("Wall");    //ステージオブジェクトを探す
	int hWall = pWall->GetModelHandle();    //モデル番号を取得

	//右
	RayCastData right;
	right.start = position_;              //レイの発射位置
	right.dir = D3DXVECTOR3(1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &right); //レイを発射

	  //レイが当たったら
	if (right.hit)
	{
		if (right.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//左
	RayCastData left;
	left.start = position_;              //レイの発射位置
	left.dir = D3DXVECTOR3(-1, 0, 0);    //レイの方向
	Model::RayCast(hWall, &left); //レイを発射

	  //レイが当たったら
	if (left.hit)
	{
		if (left.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

	//上	
	RayCastData forward;
	forward.start = position_;              //レイの発射位置
	forward.dir = D3DXVECTOR3(0, 0, 1);   //レイの方向
	Model::RayCast(hWall, &forward); //レイを発射

	  //レイが当たったら
	if (forward.hit)
	{
		if (forward.dist < 1.0f)
		{
			//position_ -= vecForward;
			position_ = prePos;
		}
	}

	//下
	RayCastData backward;
	backward.start = position_;              //レイの発射位置
	backward.dir = D3DXVECTOR3(0, 0, -1);    //レイの方向
	Model::RayCast(hWall, &backward); //レイを発射

	  //レイが当たったら
	if (backward.hit)
	{
		if (backward.dist < 1.0f)
		{
			position_ = prePos;
		}
	}

}


//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
	if (sec_ > 1 && searchStatus == 0)
	{
		Image::Draw(hPict_);
	}
}

//開放
void Player::Release()
{
}

void Player::OnCollision(IGameObject * pTarget)
{
	//HidePointの上にいる時、Eキーで隠す
	//Eキーで物が入ってた場合、"物がある" → "物を取った"(物がないとは別）状態にする
	//物が取られていた場合Resultシーン（勝利）にする。
}

//隠す側のサーチ
void Player::Search_hide()
{
	if (Input::IsKey(DIK_Q))
	{
		if (position_.x >= 18 && position_.x <= 22 &&
			position_.z <= 5 && position_.z >= -4)
		{
			if (sec_ > 60) //1秒経ったら、
			{
				//探索中画像を消す
				searchStatus = 1;
				Bed* pBed = (Bed*)FindObject("Bed");
				pBed->SetStatus(Have);
				//家具があったら、SetStatusで状態を変える。
				//探す側は家具があったら、GetStatusで見る。
				//物があったらゲームを終了させる。
			}
			sec_++;
		}
	}
	else
	{
		//sec_ = 0;
		//searchStatus = 0;
	}
}

//探す側のサーチ
void Player::Search_finding()
{
	if (Input::IsKey(DIK_R))
	{
		if (position_.x >= 18 && position_.x <= 22 &&
			position_.z <= 5 && position_.z >= -4)
		{
			if (sec_ > 60) //1秒経ったら、
			{
				//探索中画像を消す
				searchStatus = 1;
				Bed* pBed = (Bed*)FindObject("Bed");
				if (Have == pBed->GetStatus())
				{
					//探す側は家具があったら、GetStatusで見る。
					//物があったらゲームを終了させる。
					system("stop");
				}
			}
			sec_++;
		}
	}
	else
	{
		//sec_ = 0;
		//searchStatus = 0;
	}
}
