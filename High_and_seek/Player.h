#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/gameObject/Camera.h"

//Playerを管理するクラス
class Player : public IGameObject
{
	int hModel_;    //モデル番号
	int hPict_;    //画像番号
	void move(D3DXVECTOR3 &stick);
	//カメラの回転
	void CamMove(D3DXVECTOR3 &stick);

private:
	float Dash = 1;
	D3DXVECTOR3 prePos;

	time_t startTime_;	//開始時間設定
	time_t limitTime_;	//1秒計測
	int sec_;  //1の位
	int searchStatus;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	virtual void Player_move();
	virtual void HitTest();

	D3DXVECTOR3 GetPosition() { return position_; }

	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
	void Search_hide();
	void Search_finding();
};