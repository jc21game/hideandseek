#include "MenuScene.h"
#include "Engine/DirectX/Sprite.h"
#include "GameStart.h"
#include "CharacterSelect.h"
#include "OperationMethod.h"
#include "Option.h"
#include "Cursor.h"
#include "GameStartImage.h"
#include "CharacterSelectImage.h"
#include "OperationMethodImage.h"
#include "OptionImage.h"


//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene")
{
}

//初期化
void MenuScene::Initialize()
{
	CreateGameObject<GameStart>(this);
	CreateGameObject<CharacterSelect>(this);
	CreateGameObject<OperationMethod>(this);
	CreateGameObject<Option>(this);
	CreateGameObject<Cursor>(this);
}

//更新
void MenuScene::Update()
{
	//↑↓の入力による数値代入
	if (count != 3)
	{
		//↓が押されていたら
		if (Input::IsKeyDown(DIK_DOWN))
		{
			count++;
		}
	}
	if (count != 0)
	{
		//↑が押されていたら
		if (Input::IsKeyDown(DIK_UP))
		{
			count--;
		}
	}

	//現在選択してるシーンの画像表示
	switch (count)
	{
	case 0:
		CreateGameObject<GameStartImage>(this);
		break;

	case 1:
		CreateGameObject<CharacterSelectImage>(this);
		break;

	case 2:
		CreateGameObject<OptionImage>(this);
		break;

	case 3:
		CreateGameObject<OperationMethodImage>(this);
		break;
	}

	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void MenuScene::Draw()
{
}

//開放
void MenuScene::Release()
{
}