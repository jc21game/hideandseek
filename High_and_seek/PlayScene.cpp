#include "PlayScene.h"
#include "Engine/global.h"
#include "Player.h"
#include "Stage.h"
#include "Engine/gameObject/Camera.h"



PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), pText_(nullptr), pMathText_(nullptr), initFlag_(false)
{
	timer_.min_  = 0;
	timer_.tens_ = 0;
	timer_.sec_  = 0;
}

void PlayScene::Initialize()
{
	CreateGameObject<Stage>(this);
	//CreateGameObject<Player>(this);

	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
	pMathText_ = new Text("ＭＳ ゴシック", 64);

	//現在時刻(秒)
	time(&startTime_);

	//文字列の結合(timer_.min_ + timer_.tens_ + timer_.sec_ = str)
	str.append(number_[timer_.min_]);
	str.append(number_[timer_.tens_]);
	str.append(number_[timer_.sec_]);

	//プレイシーンの子供としてプレイヤーを表示
	CreateGameObject<Player>(this);

}

//更新
void PlayScene::Update()
{
	switch (state_)
	{
	case STATE_HIDETURN:
		//初期化されていなかったら
		if (!initFlag_)
		{
			SetHideTime();
		}
		

		switch (statehide_)
		{
		case STATE_HIDEPLAYER_MOVE:

			break;
		}

		//隠すプレイヤーの処理

		if (timer_.min_ == 0 && timer_.tens_ == 0 && timer_.sec_ == 0)
		{
			ChangeState(STATE_FINDTURN);
		}
		
		break;
	case STATE_FINDTURN:
		//初期化されていなかったら
		if (!initFlag_)
		{
			SetFindTime();
		}

		//switch (statefind_)
		//{
		//case STATE_FINDPLAYER_MOVE:
		//	break;
		//}

		//探すプレイヤーの処理

		if (timer_.min_ == 0 && timer_.tens_ == 0 && timer_.sec_ == 0)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_TITLE);
			//ChangeState(STATE_HIDETURN);
		}
		
	break;
	}
	//時間を計測する関数
	CountTimer();
}


///////////時間を計測///////////
void PlayScene::CountTimer()
{
	//経過時間
	limitTime_ = time(&limitTime_);

	//経過時間が１秒以上なら
	if ((limitTime_ - startTime_) >= 1)
	{
		//開始時間を計測
		startTime_ = limitTime_;

		//文字列を削除
		str.clear();

		//一の位が０になったら
		if (timer_.sec_ == 0)
		{
			//十の位が０だったら
			if (timer_.tens_ == 0)
			{
				timer_.min_--;     //百の位をデクリメント
				timer_.tens_ = 9;  //十の位を９にセット
			}
			//十の位が0じゃなかったら
			else
			{
				timer_.tens_--;    //十の位をデクリメント
			}

			timer_.sec_ = 9;       //一の位を９にセット
		}
		//一の位が０じゃなかったら
		else if (timer_.sec_ != 0)
		{
			timer_.sec_--;         //一の位をデクリメント
		}

		//それぞれの値の文字列を格納
		str.append(number_[timer_.min_]);
		str.append(number_[timer_.tens_]);
		str.append(number_[timer_.sec_]);
	}

	////全ての位が０になったら
	//if (timer_.min_ == 0 && timer_.tens_ == 0 && timer_.sec_ == 0)
	//{
	//	//タイトルシーンへ移動（取り合えず今は）
	//	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	//	pSceneManager->ChangeScene(SCENE_ID_TITLE);
	//}
}

void PlayScene::Draw()
{
	pMathText_->Draw(960,100, str);
	pText_->Draw(100, 100, "これはプレイ用のシーンです。");
	pText_->Draw(100, 150, "WASD:移動"); 
	pText_->Draw(100, 200, "SPACE:上昇");
	pText_->Draw(100, 250, "SHIFT:下降");
}

void PlayScene::Release()
{
	delete pText_;
	delete pMathText_;
}

//隠す側の時間初期化
void PlayScene::SetHideTime()
{
	timer_.min_  = 0;
	timer_.tens_ = 1;
	timer_.sec_  = 0;

	initFlag_ = true;
}

//探す側の時間初期化
void PlayScene::SetFindTime()
{
	timer_.min_  = 0;
	timer_.tens_ = 2;
	timer_.sec_  = 0;

	initFlag_ = true;
}

void PlayScene::ChangeState(STATE state)
{
	state_ = state;

	initFlag_ = false;
}
//
//void PlayScene::CreateHidePlayer()
//{
//}
//
//void PlayScene::CreateFindPlayer()
//{
//}
//

