#include "SelectScene.h"
#include "Engine/global.h"

SelectScene::SelectScene(IGameObject * parent)
	: IGameObject(parent, "SelectScene"), pText_(nullptr)
{
}

void SelectScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
}

void SelectScene::Update()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_MODE);
	}

	if (Input::IsKeyDown(DIK_O))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_OPTION);
	}

}

void SelectScene::Draw()
{
	pText_->Draw(100, 100, "これはSELECT用のシーンです。Oでオプション、SPACEで次のモード選択");
}

void SelectScene::Release()
{
	delete pText_;
}
