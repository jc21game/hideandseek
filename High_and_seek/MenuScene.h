#pragma once
#include "Engine/GameObject/GameObject.h"

class Camera;


//プレイシーンを管理するクラス
class MenuScene : public IGameObject
{
public:
	int count;
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MenuScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};