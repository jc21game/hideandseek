#include "CharacterSelectScene.h"
#include "Engine/global.h"

CharacterSelectScene::CharacterSelectScene(IGameObject * parent)
	: IGameObject(parent, "CharacterSelectScene"), pText_(nullptr)
{
}

void CharacterSelectScene::Initialize()
{
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 32);
}

void CharacterSelectScene::Update()
{
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

void CharacterSelectScene::Draw()
{
	pText_->Draw(100, 100, "キャラクター選択シーン");
}

void CharacterSelectScene::Release()
{
	delete pText_;
}