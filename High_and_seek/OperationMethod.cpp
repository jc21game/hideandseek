#include "OperationMethod.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
OperationMethod::OperationMethod(IGameObject * parent)
	:IGameObject(parent, "OperationMethod"), hPict_(-1)
{
}

//デストラクタ
OperationMethod::~OperationMethod()
{
}

//初期化
void OperationMethod::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/OperationMethod.png");
	assert(hPict_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(1600.0f, 800.0f, 0);
}

//更新
void OperationMethod::Update()
{
}

//描画
void OperationMethod::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OperationMethod::Release()
{
}