#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"
#include "Engine/ResouceManager/Image.h"
#include "House.h"
#include "escapePlayer.h"
#include "DemonPlayer.h"
#include "furniture.h"

//#define twoHigh 10

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	StageObject();

	Player* pPlayer_ = CreateGameObject<Player>(this);
	pPlayer_->SetPosition(D3DXVECTOR3(10, 1, -5));

}

void Stage::StageObject()
{
	furniture();

	House* pHouse;
	pHouse = CreateGameObject<House>(this);

	Wall* pWall;
	pWall = CreateGameObject<Wall>(this);

	//DemonPlayer* pDemonPlayer;
	//pDemonPlayer = CreateGameObject<DemonPlayer>(this);
	//pDemonPlayer->SetPosition(D3DXVECTOR3(15.0f, 1, 8));
	//pDemonPlayer->SetRotateY(180.0f);

	//EscapePlayer* pEscapePlayer;
	//pEscapePlayer = CreateGameObject<EscapePlayer>(this);
	//pEscapePlayer->SetPosition(D3DXVECTOR3(17.0f, 0.8, 8));
	//pEscapePlayer->SetRotateY(180.0f);
}

void Stage::furniture()
{
	Chair* pChair;
	pChair = CreateGameObject<Chair>(this);
	pChair->SetPosition(D3DXVECTOR3(8, 1, 8));
	pChair->SetScale(D3DXVECTOR3(0.5f, 0.5f, 0.5f));
	pChair->SetRotateY(180.0f);

	Clock* pClock;
	pClock = CreateGameObject<Clock>(this);
	pClock->SetPosition(D3DXVECTOR3(10, 3, 8));
	pClock->SetRotateY(180.0f);

	Bed* pBed;
	pBed = CreateGameObject<Bed>(this);
	pBed->SetPosition(D3DXVECTOR3(33, 1, 33));

}



//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
}

//開放
void Stage::Release()
{
}