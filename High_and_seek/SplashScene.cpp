#include "SplashScene.h"
#include "Engine/global.h"
#include "Engine/ResouceManager/Movie.h"

SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene")
{
}

void SplashScene::Initialize()
{
	//モデルデータのロード
	Movie::Load(L"data/TeamLogo/title.avi");
	Movie::Play();

	//Direct3Dを止める
	//Sleep(Movie::GetMovieLength());
}

void SplashScene::Update()
{
	OAFilterState Status = Movie::MovieState();

	if (Input::IsKeyDown(DIK_RETURN) || Status == 2)
	{
		Movie::Stop();
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_LOGO);
	}
}

void SplashScene::Draw()
{	
}

void SplashScene::Release()
{
}
