#include "OperationMethodImage.h"
#include "Engine/ResouceManager/Image.h"
#include "MenuScene.h"

//コンストラクタ
OperationMethodImage::OperationMethodImage(IGameObject * parent)
	:IGameObject(parent, "OperationMethodImage"), hPict_(-1)
{
}

//デストラクタ
OperationMethodImage::~OperationMethodImage()
{
}

//初期化
void OperationMethodImage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/MenuIcon/OperationMethodImage.png");
	assert(hPict_ >= 0);
}

//更新
void OperationMethodImage::Update()
{
	//↓が押されていたら
	if (Input::IsKeyDown(DIK_DOWN))
	{
		count++;
	}
	//↑が押されていたら
	if (Input::IsKeyDown(DIK_UP))
	{
		count++;
	}

	//画像削除
	if (count == 2)
	{
		count = 0;
		KillMe();
	}
}

//描画
void OperationMethodImage::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OperationMethodImage::Release()
{
}