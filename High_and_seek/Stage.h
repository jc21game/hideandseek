#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Player.h"

//ステージを管理するクラス
class Stage : public IGameObject
{

	D3DXVECTOR3 Pos;
	Player* pPlayer_;

private:
	int _hPict;    //画像番号

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void StageObject();

	void furniture();
};